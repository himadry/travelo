<?php

function travelo_add_custom_user_profile_fields( $user ) {
?>
	<h3><?php _e('Personal Information', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<!-- <tr>
			<th>
				<label for="address"><?php _e('Address', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="address" id="address" value="<?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr> -->

		<tr>
			<th>
				<label for="middle_initial"><?php _e('Middle Initial', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="middle_initial" id="middle_initial" value="<?php echo esc_attr( get_the_author_meta( 'middle_initial', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="gender"><?php _e('Gender', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="gender" id="gender" value="<?php echo esc_attr( get_the_author_meta( 'gender', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="nationality"><?php _e('Nationality', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="nationality" id="nationality" value="<?php echo esc_attr( get_the_author_meta( 'nationality', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="passport_number"><?php _e('Passport Number', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="passport_number" id="passport_number" value="<?php echo esc_attr( get_the_author_meta( 'passport_number', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="dietary_req"><?php _e('Dietary Requirements', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="dietary_req" id="dietary_req" value="<?php echo esc_attr( get_the_author_meta( 'dietary_req', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="allergies"><?php _e('Allergies', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="allergies" id="allergies" value="<?php echo esc_attr( get_the_author_meta( 'allergies', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>

	<h3><?php _e('Contact Details', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="home_address"><?php _e('Home Address', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="home_address" id="home_address" value="<?php echo esc_attr( get_the_author_meta( 'home_address', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="hotel_name"><?php _e('Hotel Name', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="hotel_name" id="hotel_name" value="<?php echo esc_attr( get_the_author_meta( 'hotel_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="hotel_name"><?php _e('Date Check in', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="date_check" id="date_check" value="<?php echo esc_attr( get_the_author_meta( 'date_check', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>

	<h3><?php _e('Emergency Contact Details', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="emergency_name"><?php _e('Name', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="emergency_name" id="emergency_name" value="<?php echo esc_attr( get_the_author_meta( 'emergency_name', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="emergency_relationship"><?php _e('Relationship', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="emergency_relationship" id="emergency_relationship" value="<?php echo esc_attr( get_the_author_meta( 'emergency_relationship', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="emergency_mobile_no"><?php _e('Mobile Phone', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="emergency_mobile_no" id="emergency_mobile_no" value="<?php echo esc_attr( get_the_author_meta( 'emergency_mobile_no', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th>
				<label for="emergency_home_phone"><?php _e('Home Phone', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="emergency_home_phone" id="emergency_home_phone" value="<?php echo esc_attr( get_the_author_meta( 'emergency_home_phone', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
	</table>

	<h3><?php _e('Diving Details', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="diving_certificate_agency"><?php _e('Certification Agency', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="diving_certificate_agency" id="diving_certificate_agency" value="<?php echo esc_attr( get_the_author_meta( 'diving_certificate_agency', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="diving_certification_level"><?php _e('Certification Level', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="diving_certification_level" id="diving_certification_level" value="<?php echo esc_attr( get_the_author_meta( 'diving_certification_level', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="number_of_dives"><?php _e('Number of Dives', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="number_of_dives" id="number_of_dives" value="<?php echo esc_attr( get_the_author_meta( 'number_of_dives', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th>
				<label for="date_of_last_dive"><?php _e('Date of Last Dive', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="date_of_last_dive" id="date_of_last_dive" value="<?php echo esc_attr( get_the_author_meta( 'date_of_last_dive', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th>
				<label for="prior_dsc"><?php _e('Prior DCS', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="Yes" <?php if(get_user_meta( $user->ID, 'prior_dsc', true )== 'Yes') {echo 'checked';}  ?> />Yes
	            <input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="No" <?php if (get_user_meta( $user->ID, 'prior_dsc', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>
	</table>

	<h3><?php _e('Insurance', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="have_dive_insurance"><?php _e('Do you have dive insurance?', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="Yes" <?php if(get_user_meta( $user->ID, 'have_dive_insurance', true )== 'Yes') {echo 'checked';}  ?> />Yes
	            <input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="No" <?php if (get_user_meta( $user->ID, 'have_dive_insurance', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>

		<tr>
			<th>
				<label for="dive_insurance_no"><?php _e('Dive insurance number', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="dive_insurance_no" id="dive_insurance_no" value="<?php echo esc_attr( get_the_author_meta( 'dive_insurance_no', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="need_dive_insurance"><?php _e('Do you need dive insurance?', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="Yes" <?php if(get_user_meta( $user->ID, 'need_dive_insurance', true )== 'Yes') {echo 'checked';}  ?> />Yes
	            <input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="No" <?php if (get_user_meta( $user->ID, 'need_dive_insurance', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>		
	</table>

	<h3><?php _e('Equipment Information', 'your_textdomain'); ?></h3>
	<table class="form-table">
		<tr>
			<th>
				<label for="rent_diving_equipment"><?php _e('Do you want to rent diving equipment?', 'your_textdomain'); ?>
			</label>
			</th>
			<td>
				<input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="Yes" <?php if(get_user_meta( $user->ID, 'rent_diving_equipment', true )== 'Yes') {echo 'checked';}  ?> />Yes
	            <input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="No" <?php if (get_user_meta( $user->ID, 'rent_diving_equipment', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>

		<tr>
			<th>
				<label for="BCD"><?php _e('BCD', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXS" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'XXS')  {echo 'checked';} ?> />XXS
		        <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XS" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'XS')  {echo 'checked';} ?> />XS
		        <input type="radio" name="size_bcd" id="size_bcd" class="input" value="S" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'S')  {echo 'checked';} ?> />S
		        <input type="radio" name="size_bcd" id="size_bcd" class="input" value="M" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'M')  {echo 'checked';} ?> />M
		        <input type="radio" name="size_bcd" id="size_bcd" class="input" value="L" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'L')  {echo 'checked';} ?> />L
		        <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XL" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'XL')  {echo 'checked';} ?> />XL
		        <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXL" <?php if (get_user_meta( $user->ID, 'size_bcd', true )== 'XXL')  {echo 'checked';} ?> />XXL
			</td>
		</tr>

		<tr>
			<th>
				<label for="Wetsuit"><?php _e('Wetsuit', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="size_websuit" id="size_websuit" class="input" value="XS" <?php if (get_user_meta( $user->ID, 'size_websuit', true )== 'XS')  {echo 'checked';} ?> />XS
		        <input type="radio" name="size_websuit" id="size_websuit" class="input" value="S" <?php if (get_user_meta( $user->ID, 'size_websuit', true )== 'S')  {echo 'checked';} ?>/>S
		        <input type="radio" name="size_websuit" id="size_websuit" class="input" value="M" <?php if (get_user_meta( $user->ID, 'size_websuit', true )== 'M')  {echo 'checked';} ?> />M
		        <input type="radio" name="size_websuit" id="size_websuit" class="input" value="L" <?php if (get_user_meta( $user->ID, 'size_websuit', true )== 'L')  {echo 'checked';} ?> />L
		        <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XL" <?php if (get_user_meta( $user->ID, 'size_websuit', true )== 'XL')  {echo 'checked';} ?> />XL
		        <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XXL"  <?php if (get_user_meta( $user->ID, 'size_websuit', true )== 'XXL')  {echo 'checked';} ?>/>XXL
			</td>
		</tr>

		<tr>
			<th>
				<label for="Fins"><?php _e('Fins', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="size_fins" id="size_fins" class="input" value="36-37" <?php if (get_user_meta( $user->ID, 'size_fins', true )== '36-37')  {echo 'checked';} ?> />36-37
		        <input type="radio" name="size_fins" id="size_fins" class="input" value="38-39" <?php if (get_user_meta( $user->ID, 'size_fins', true )== '38-39')  {echo 'checked';} ?> />38-39
		        <input type="radio" name="size_fins" id="size_fins" class="input" value="40-41" <?php if (get_user_meta( $user->ID, 'size_fins', true )== '40-41')  {echo 'checked';} ?> />40-41
		        <input type="radio" name="size_fins" id="size_fins" class="input" value="42-43" <?php if (get_user_meta( $user->ID, 'size_fins', true )== '42-43')  {echo 'checked';} ?> />42-43
		        <input type="radio" name="size_fins" id="size_fins" class="input" value="44-45" <?php if (get_user_meta( $user->ID, 'size_fins', true )== '44-45')  {echo 'checked';} ?> />44-45
			</td>
		</tr>

		<tr>
			<th>
				<label for="regulator"><?php _e('Regulator', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="regulator" id="regulator" class="input" value="Yes" <?php if (get_user_meta( $user->ID, 'regulator', true )== 'Yes')  {echo 'checked';} ?> />Yes
	            <input type="radio" name="regulator" id="regulator" class="input" value="No" <?php if (get_user_meta( $user->ID, 'regulator', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>
		<tr>
			<th>
				<label for="Mask"><?php _e('Mask', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="mask" id="mask" class="input" value="Yes" <?php if (get_user_meta( $user->ID, 'mask', true )== 'Yes')  {echo 'checked';} ?> />Yes
	            <input type="radio" name="mask" id="mask" class="input" value="No" <?php if (get_user_meta( $user->ID, 'mask', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>

		<tr>
			<th>
				<label for="Weight"><?php _e('Weight', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="weight" id="weight" value="<?php echo esc_attr( get_the_author_meta( 'weight', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="height"><?php _e('Height', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="height" id="height" value="<?php echo esc_attr( get_the_author_meta( 'height', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="shoe_size"><?php _e('Shoe Size (EU)', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="shoe_size" id="shoe_size" value="<?php echo esc_attr( get_the_author_meta( 'shoe_size', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

		<tr>
			<th>
				<label for="need_din"><?php _e('Do you need a DIN?', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="need_din" id="need_din" class="input" value="Yes" <?php if (get_user_meta( $user->ID, 'need_din', true )== 'Yes')  {echo 'checked';} ?> />Yes
	            <input type="radio" name="need_din" id="need_din" class="input" value="No" <?php if (get_user_meta( $user->ID, 'need_din', true )== 'No')  {echo 'checked';} ?> />No
			</td>
		</tr>

		<tr>
			<th>
				<label for="rent_dive_computer"><?php _e('Do you want to rent a Dive Computer?', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="Yes" <?php if (get_user_meta( $user->ID, 'rent_dive_computer', true )== 'Yes')  {echo 'checked';} ?>/>Yes
	            <input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="No" <?php if (get_user_meta( $user->ID, 'rent_dive_computer', true )== 'No')  {echo 'checked';} ?>/>No
			</td>
		</tr>

		<tr>
			<th>
				<label for="optional_course"><?php _e('Courses (optional)', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="radio" name="optional_course" id="optional_course" class="input" value="DSD" <?php if (get_user_meta( $user->ID, 'optional_course', true )== 'DSD')  {echo 'checked';} ?> />DSD (Discover Scuba Diving)	
						 				 
		        <input type="radio" name="optional_course" id="optional_course" class="input" value="Refresher" <?php if (get_user_meta( $user->ID, 'optional_course', true )== 'Refresher')  {echo 'checked';} ?> />Refresher

		        <input type="radio" name="optional_course" id="optional_course" class="input" value="Open Water Course" <?php if (get_user_meta( $user->ID, 'optional_course', true )== 'Open Water Course')  {echo 'checked';} ?> />Open Water Course

		        <input type="radio" name="optional_course" id="optional_course" class="input" value="Advanced Open Water Course" <?php if (get_user_meta( $user->ID, 'optional_course', true )== 'Advanced Open Water Course')  {echo 'checked';} ?> />Advanced Open Water Course

		        <input type="radio" name="optional_course" id="optional_course" class="input" value="Adventure Deep Dive" <?php if (get_user_meta( $user->ID, 'optional_course', true )== 'Adventure Deep Dive')  {echo 'checked';} ?> />Adventure Deep Dive

		        <input type="radio" name="optional_course" id="optional_course" class="input" value="Specialty" <?php if (get_user_meta( $user->ID, 'optional_course', true )== 'Specialty')  {echo 'checked';} ?> />Specialty
			</td>
		</tr>
				
	</table>

<?php }

function travelo_save_custom_user_profile_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	
	update_usermeta( $user_id, 'address', $_POST['address'] );
	update_usermeta( $user_id, 'middle_initial', $_POST['middle_initial'] );
	update_usermeta( $user_id, 'gender', $_POST['gender'] );
	update_usermeta( $user_id, 'nationality', $_POST['nationality'] );
	update_usermeta( $user_id, 'passport_number', $_POST['passport_number'] );
	update_usermeta( $user_id, 'dietary_req', $_POST['dietary_req'] );
	update_usermeta( $user_id, 'allergies', $_POST['allergies'] );
	update_usermeta( $user_id, 'home_address', $_POST['home_address'] );
	update_usermeta( $user_id, 'hotel_name', $_POST['hotel_name'] );
	update_usermeta( $user_id, 'emergency_name', $_POST['emergency_name'] );
	update_usermeta( $user_id, 'emergency_relationship', $_POST['emergency_relationship'] );
	update_usermeta( $user_id, 'emergency_mobile_no', $_POST['emergency_mobile_no'] );
	update_usermeta( $user_id, 'emergency_home_phone', $_POST['emergency_home_phone'] );
	update_usermeta( $user_id, 'diving_certificate_agency', $_POST['diving_certificate_agency'] );
	update_usermeta( $user_id, 'diving_certification_level', $_POST['diving_certification_level'] );
	update_usermeta( $user_id, 'number_of_dives', $_POST['number_of_dives'] );
	update_usermeta( $user_id, 'date_of_last_dive', $_POST['date_of_last_dive'] );
	update_usermeta( $user_id, 'prior_dsc', $_POST['prior_dsc'] );
	update_usermeta( $user_id, 'have_dive_insurance', $_POST['have_dive_insurance'] );
	update_usermeta( $user_id, 'dive_insurance_no', $_POST['dive_insurance_no'] );
	update_usermeta( $user_id, 'need_dive_insurance', $_POST['need_dive_insurance'] );
	update_usermeta( $user_id, 'rent_diving_equipment', $_POST['rent_diving_equipment'] );
	update_usermeta( $user_id, 'size_bcd', $_POST['size_bcd'] );
	update_usermeta( $user_id, 'size_websuit', $_POST['size_websuit'] );
	update_usermeta( $user_id, 'size_fins', $_POST['size_fins'] );
	update_usermeta( $user_id, 'regulator', $_POST['regulator'] );
	update_usermeta( $user_id, 'mask', $_POST['mask'] );
	update_usermeta( $user_id, 'weight', $_POST['weight'] );
	update_usermeta( $user_id, 'height', $_POST['height'] );
	update_usermeta( $user_id, 'shoe_size', $_POST['shoe_size'] );
	update_usermeta( $user_id, 'need_din', $_POST['need_din'] );
	update_usermeta( $user_id, 'rent_dive_computer', $_POST['rent_dive_computer'] );
	update_usermeta( $user_id, 'optional_course', $_POST['optional_course'] );	
	
	
}

add_action( 'show_user_profile', 'travelo_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'travelo_add_custom_user_profile_fields' );

add_action( 'personal_options_update', 'travelo_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'travelo_save_custom_user_profile_fields' );