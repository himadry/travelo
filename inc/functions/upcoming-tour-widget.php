<?php 
	
	// Creating the widget 
	class upcoming_tour_widget extends WP_Widget {

	function __construct() {
	parent::__construct(
	// Base ID of your widget
	'upcoming_tour_widget', 

	// Widget name will appear in UI
	__('Upcoming Tours', 'upcoming_tour_widget_domain'), 

	// Widget description
	array( 'description' => __( 'Show upcoming tour', 'upcoming_tour_widget_domain' ), ) 
	);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	$number_of_post = (int) $instance['number_of_post'];
	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) )
	echo $args['before_title'] . $title . $args['after_title'];

	// This is where you run the code and display the output
	
	$tour_id = 1321;global $wpdb;
	$today = date("Y-m-d");
	// TRAV_TOUR_SCHEDULES_TABLE;
	$tbl_posts = esc_sql( $wpdb->posts );
	$upcoming_date_ids = "SELECT tour_id FROM ".TRAV_TOUR_SCHEDULES_TABLE." WHERE tour_date > '".$today."'";
	
    $results = $wpdb->get_results( $upcoming_date_ids );
    $post_ids = array();
	foreach ($results as $result) {
		 $post_ids[] = $result->tour_id;
	}
	$unique_post_ids = array_unique( $post_ids) ;
    //print_r($unique_post_ids);

	$count = 0;
	foreach ($unique_post_ids as  $tour_id) {
		if($number_of_post == $count) break;
	    echo '<div class="image-box style14">
	    		<article class="box upcoming">
	    			<figure><a href="' . esc_url( get_permalink( $tour_id ) ) . '">' . get_the_post_thumbnail( $tour_id, array( $thumb_width, $thumb_height ) ) . '</a>
	    			</figure>
	    			<div class="details">
	    			<h5 class="title">
	    			<a href="' . esc_url( get_permalink( $tour_id ) ) . '">' . esc_html( get_the_title( $tour_id ) ) . '</a>
	    			</h5>
	    			<label class="price-wrapper">
	    			<span class="price-per-unit">฿' . get_post_meta($tour_id, 'trav_tour_min_price',true) . '</span> ' . __( 'per person', 'trav' ) . '
	    			</label>
	    			</div>
	    		</article> 
	    	</div>';

	    $count = $count+1;
	   
	}

	echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
	$title = $instance[ 'title' ];
	}
	else {
	$title = __( 'Upcoming Tours', 'upcoming_tour_widget_domain' );
	}
	if ( isset( $instance[ 'number_of_post' ] ) ) {
	$number_of_post = $instance[ 'number_of_post' ];
	}
	else {
	$number_of_post = 5;
	}
	// Widget admin form
	?>
	<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<p>
	<label for="<?php echo $this->get_field_id( 'number_of_post' ); ?>"><?php _e( 'Number of post:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_post' ); ?>" name="<?php echo $this->get_field_name( 'number_of_post' ); ?>" type="text" value="<?php echo esc_attr( $number_of_post ); ?>" />
	</p>
	<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	$instance = array();
	$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	$instance['number_of_post'] = (int) $new_instance['number_of_post'];
	return $instance;
	}
	} // Class wpb_widget ends here

	// Register and load the widget
	function upcoming_tour_load_widget() {
		register_widget( 'upcoming_tour_widget' );
	}
	add_action( 'widgets_init', 'upcoming_tour_load_widget' );		

 ?>