<?php 
add_action( 'init', 'booking_posttype_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function booking_posttype_init() {
	$labels = array(
		'name'               => _x( 'Booking', 'post type general name', 'trav' ),
		'singular_name'      => _x( 'Booking', 'post type singular name', 'trav' ),
		'menu_name'          => _x( 'Bookings', 'admin menu', 'trav' ),
		'name_admin_bar'     => _x( 'Booking', 'add new on admin bar', 'trav' ),
		'add_new'            => _x( 'Add New', 'book', 'trav' ),
		'add_new_item'       => __( 'Add New Booking', 'trav' ),
		'new_item'           => __( 'New Booking', 'trav' ),
		'edit_item'          => __( 'Edit Booking', 'trav' ),
		'view_item'          => __( 'View Booking', 'trav' ),
		'all_items'          => __( 'All Bookings', 'trav' ),
		'search_items'       => __( 'Search Bookings', 'trav' ),
		'parent_item_colon'  => __( 'Parent Bookings:', 'trav' ),
		'not_found'          => __( 'No bookings found.', 'trav' ),
		'not_found_in_trash' => __( 'No bookings found in Trash.', 'trav' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'trav' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'booking' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' )
	);

	register_post_type( 'booking', $args );
}

/*************************
##
**************************/
add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );
function remove_row_actions( $actions )
{
    if( get_post_type() === 'booking' )
        unset( $actions['edit'] );
        unset( $actions['view'] );
        unset( $actions['trash'] );
        unset( $actions['inline hide-if-no-js'] );
    return $actions;
}
/***************************
 *## Booking meta box 
**************************/
	function booking_meta_box() {

		$screens = array( 'booking' );

		foreach ( $screens as $screen ) {

			add_meta_box(
				'booking_sectionid',
				__( 'Booking Information', 'booking_textdomain' ),
				'booking_meta_box_callback',
				$screen
			);
		}
	}
	add_action( 'add_meta_boxes', 'booking_meta_box' );

	/**
	 * Prints the box content.
	 * 
	 * @param WP_Post $post The object for the current post/page.
	 */
		function booking_meta_box_callback( $post ) {

			// Add an nonce field so we can check for it later.
			wp_nonce_field( 'booking_meta_box', 'booking_meta_box_nonce' );

			/*
			 * Use get_post_meta() to retrieve an existing value
			 * from the database and use the value for the form.
			 */
			
			$first_name = get_post_meta( $post->ID, 'first_name', true );
			$last_name = get_post_meta( $post->ID, 'last_name', true );
			$email = get_post_meta( $post->ID, 'email', true );	
			$country_code = get_post_meta( $post->ID, 'country_code', true );			
			$phone = get_post_meta( $post->ID, 'phone', true );	
			$address = get_post_meta( $post->ID, 'address', true );	
			$city = get_post_meta( $post->ID, 'city', true );	
			$zip = get_post_meta( $post->ID, 'zip', true );	
			$country = get_post_meta( $post->ID, 'country', true );	
			$certification_level = get_post_meta( $post->ID, 'certification_level', true );	
			$special_requirements = get_post_meta( $post->ID, 'special_requirements', true );	
			$booking_pin_code = get_post_meta( $post->ID, 'booking_pin_code', true );	
			$booking_no = get_post_meta( $post->ID, 'booking_no', true );	
			$booking_user_id = get_post_meta( $post->ID, 'booking_user_id', true );	
			$trip_destination = get_post_meta( $post->ID, 'trip_destination', true );	
			$trip_boat_name = get_post_meta( $post->ID, 'trip_boat_name', true );	
			$trip_date = get_post_meta( $post->ID, 'trip_date', true );	
			$trip_duration = get_post_meta( $post->ID, 'trip_duration', true );	
			$number_of_people = get_post_meta( $post->ID, 'number_of_people', true );	
			$cabin_preferences = get_post_meta( $post->ID, 'cabin_preferences', true );	
			$bed_configaration = get_post_meta( $post->ID, 'bed_configaration', true );
			$price_range = get_post_meta( $post->ID, 'price_range', true );
			$deck_location = get_post_meta( $post->ID, 'deck_location', true );	
	
            if($first_name) {			
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'FIRST NAME: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $first_name ).'</div>';
				echo '</div>';			

				echo '<div class="clear"></div>';
			}
			if( $last_name){
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left;font-size:17px; font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'LAST NAME:  ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $last_name ).'</div>';
				echo '</div>';

			 	echo '<div class="clear"></div>';
		    }
		    if($email){
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'EMAIL: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $email ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
		    }
		    if($country_code){
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'COUNTRY CODE: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $country_code ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($phone){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'PHONE NUMBER: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $phone ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($address){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'ADDRESS: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $address ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($city){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'CITY: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $city ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($zip){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'ZIP CODE: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $zip ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($country){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'COUNTRY: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $country ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($certification_level){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'CERTIFICATION LEVEL: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $certification_level ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($special_requirements){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'SPECIAL REQUIREMENTS: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $special_requirements ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($booking_pin_code){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'BOOKING PIN CODE: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $booking_pin_code ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($booking_no){
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'BOOKING NO: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $booking_no ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($booking_user_id){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'BOOKING USER ID: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $booking_user_id ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($trip_destination){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'DESTINATION: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $trip_destination ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($trip_boat_name)	{
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'BOAT NAME: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $trip_boat_name ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($trip_date){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'TRIP DATE: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $trip_date ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($trip_duration){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'TRIP DURATION: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $trip_duration ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($number_of_people){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'NUMBER OF PEOPLE: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $number_of_people ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($cabin_preferences){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'CABIN PREFERENCES: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $cabin_preferences ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($bed_configaration){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'BED CONFIGURATION: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $bed_configaration ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
		    }
		    if($price_range){			
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'QUALITY/PRICE RANGE: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $price_range ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
			if($deck_location){	
				echo '<div style="line-height: 2.2em;" class="field_lebel"><label style="float:left; font-size:17px;font-weight:bold;width:210px;text-align:right;" for="Name">';
				_e( 'LOCATION: ', 'order_textdomain' );
				echo '</label>';
				echo '<div style="width:30%;float:left;padding-left: 6px;font-size: 17px" class="field_info">'.esc_attr( $deck_location ).'</div>';
				echo '</div>';

				echo '<div class="clear"></div>';
			}
		}

 ?>