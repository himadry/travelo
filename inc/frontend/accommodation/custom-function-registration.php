<?php 
//Add custom style for custom registration page
function travelo_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/custom-registration.css' );
    //wp_enqueue_script( 'custom-login', get_template_directory_uri() . '/style-login.js' );
   ?> 
    <style>
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/Logo-4.png);
            padding-bottom: 30px;
            background-size: 200px;
            width: 100%;
        }        
    </style>
<?php
}
add_action( 'login_enqueue_scripts', 'travelo_login_stylesheet' );

//Logo url
function travelo_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'travelo_login_logo_url' );

function travelo_login_logo_url_title() {
    return 'Travelo';
}
add_filter( 'login_headertitle', 'travelo_login_logo_url_title' );
//end logo url

//Redirect url after login
function travelo_login_redirect( $url, $request, $user ){
    if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
        if( $user->has_cap( 'administrator' ) ) {
            $url = admin_url();
        } 
    }
    return $url;
}
add_filter('login_redirect', 'travelo_login_redirect', 10, 3 );

function redirect_to_current_page() {
    global $redirect_to;
    if (isset($_GET['redirect_to'])) {

            $redirect_to = $_GET['redirect_to'];
    }
    else{
            $redirect_to = home_url();    
    }

    return $redirect_to;
}

add_action('login_form', 'redirect_to_current_page');

/*************************
## Custom registation form 
**************************/

//1. Add a new form element
add_action( 'register_form', 'travelo_register_form' );
function travelo_register_form() {

    /*PERSONAL INFORMATION*/            
        if(isset($_POST['first_name'])){
            $first_name = trim($_POST['first_name']);
        }       
        if(isset($_POST['middle_initial'])){
            $middle_initial = trim($_POST['middle_initial']);
        }        
        if(isset($_POST['last_name'])){
            $last_name = trim($_POST['last_name']);
        }
        if(isset($_POST['gender'])){
            $gender = trim($_POST['gender']);
        }
        if(isset($_POST['nationality'])){
            $nationality = trim($_POST['nationality']);
        }
        if(isset($_POST['birthday'])){
            $birthday = trim($_POST['birthday']);
        }
        if(isset($_POST['passport_number'])){
            $passport_number = trim($_POST['passport_number']);
        }
        if(isset($_POST['dietary_req'])){
            $dietary_req = trim($_POST['dietary_req']);
        }
        if(isset($_POST['allergies'])){
            $allergies = trim($_POST['allergies']);
        }

        /*CONTACT DETAILS*/
        if(isset($_POST['home_address'])){
            $home_address = trim($_POST['home_address']);
        }
        if(isset($_POST['phone'])){
            $phone = trim($_POST['phone']);
        }
        if(isset($_POST['hotel_name'])){
            $hotel_name = trim($_POST['hotel_name']);
        }
        if(isset($_POST['date_check'])){
            $date_check = trim($_POST['date_check']);
        }

        /*EMERGENCY CONTACT DETAILS*/
        if(isset($_POST['emergency_name'])){
            $emergency_name = trim($_POST['emergency_name']);
        }
        if(isset($_POST['emergency_relationship'])){
            $emergency_relationship = trim($_POST['emergency_relationship']);
        }
        if(isset($_POST['emergency_mobile_no'])){
            $emergency_mobile_no = trim($_POST['emergency_mobile_no']);
        }
        if(isset($_POST['emergency_home_phone'])){
            $emergency_home_phone = trim($_POST['emergency_home_phone']);
        }

        /*DIVING DETAILS*/
        if(isset($_POST['diving_certificate_agency'])){
            $diving_certificate_agency = trim($_POST['diving_certificate_agency']);
        }
        if(isset($_POST['diving_certification_level'])){
            $diving_certification_level = trim($_POST['diving_certification_level']);
        }
        if(isset($_POST['number_of_dives'])){
            $number_of_dives = trim($_POST['number_of_dives']);
        }
        if(isset($_POST['date_of_last_dive'])){
            $date_of_last_dive = trim($_POST['date_of_last_dive']);
        }
        if(isset($_POST['prior_dsc'])){
            $prior_dsc = trim($_POST['prior_dsc']);
        }

        /*Insurance*/       
        if(isset($_POST['have_dive_insurance'])){
            $have_dive_insurance = trim($_POST['have_dive_insurance']);
        }
        if(isset($_POST['dive_insurance_no'])){
            $dive_insurance_no = trim($_POST['dive_insurance_no']);
        }
        if(isset($_POST['need_dive_insurance'])){
            $need_dive_insurance = trim($_POST['need_dive_insurance']);
        }   

        /*EQUIPMENT INFORMATION*/   
        if(isset($_POST['rent_diving_equipment'])){
            $rent_diving_equipment = trim($_POST['rent_diving_equipment']);
        }
        if(isset($_POST['size_bcd'])){
            $size_bcd = trim($_POST['size_bcd']);
        }
        if(isset($_POST['size_websuit'])){
            $size_websuit = trim($_POST['size_websuit']);
        }
        if(isset($_POST['size_fins'])){
            $size_fins = trim($_POST['size_fins']);
        }
        if(isset($_POST['regulator'])){
            $regulator = trim($_POST['regulator']);
        }       
        if(isset($_POST['mask'])){
            $mask = trim($_POST['mask']);
        }
        if(isset($_POST['weight'])){
            $weight = trim($_POST['weight']);
        }
        if(isset($_POST['height'])){
            $height = trim($_POST['height']);
        }
        if(isset($_POST['shoe_size'])){
            $shoe_size = trim($_POST['shoe_size']);
        }
        if(isset($_POST['need_din'])){
            $need_din = trim($_POST['need_din']);
        }
        if(isset($_POST['rent_dive_computer'])){
            $rent_dive_computer = trim($_POST['rent_dive_computer']);
        }
        if(isset($_POST['optional_course'])){
            $optional_course = trim($_POST['optional_course']);
        } 


    //$vat_no = ( ! empty( $_POST['vat_no'] ) ) ? trim( $_POST['vat_no'] ) : '';
        
        ?>        
            <p>
    			<label for="password">Password<br/>
    			<input id="password" class="input" type="password" tabindex="30" size="25" value="" name="password" />
    			</label>
    		</p>
    		<p>
    			<label for="repeat_password">Repeat password<br/>
    			<input id="repeat_password" class="input" type="password" tabindex="40" size="25" value="" name="repeat_password" />
    			</label>
    		</p>

            <h3 class="info_header">PERSONAL INFORMATION</h3>
            <p>
                <label for="first_name"><?php _e( 'First Name', 'travelo' ) ?><br />
                <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="middle_initial"><?php _e( 'Middle Initial', 'travelo' ) ?><br />
                <input type="text" name="middle_initial" id="middle_initial" class="input" value="<?php echo esc_attr( wp_unslash( $middle_initial ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="last_name"><?php _e( 'Last Name', 'travelo' ) ?><br />
                <input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="gender"><?php _e( 'Gender', 'travelo' ) ?><br />
                <input type="text" name="gender" id="gender" class="input" value="<?php echo esc_attr( wp_unslash( $gender ) ); ?>" size="25" /></label>
            </p>

            <p>
                <label for="nationality"><?php _e( 'Nationality', 'travelo' ) ?><br />
                <input type="text" name="nationality" id="nationality" class="input" value="<?php echo esc_attr( wp_unslash( $nationality ) ); ?>" size="25" /></label>
            </p>

            <p>
                <label for="date_of_birth"><?php _e( 'Date of Birth', 'travelo' ) ?><br />
                <input type="date" name="date_of_birth" id="date_of_birth" class="input" value="<?php echo esc_attr( wp_unslash( $date_of_birth ) ); ?>" size="25" /></label>
            </p>

            <p>
                <label for="passport_number"><?php _e( 'Passport Number', 'travelo' ) ?><br />
                <input type="text" name="passport_number" id="passport_number" class="input" value="<?php echo esc_attr( wp_unslash( $passport_number ) ); ?>" size="25" /></label>
            </p>

            <p>
                <label for="dietary_req"><?php _e( 'Dietary Requirements', 'travelo' ) ?><br />
                <input type="text" name="dietary_req" id="dietary_req" class="input" value="<?php echo esc_attr( wp_unslash( $dietary_req ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="allergies"><?php _e( 'Allergies', 'travelo' ) ?><br />
                <input type="text" name="allergies" id="allergies" class="input" value="<?php echo esc_attr( wp_unslash( $allergies ) ); ?>" size="25" /></label>
            </p>
            <div class="clear"></div>

            <h3 class="info_header">CONTACT DETAILS</h3>
            <p>
                <label for="home_address"><?php _e( 'Home Address', 'travelo' ) ?><br />
                <input type="text" name="home_address" id="home_address" class="input" value="<?php echo esc_attr( wp_unslash( $home_address ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="phone"><?php _e( 'Mobile Phone', 'travelo' ) ?><br />
                <input type="text" name="phone" id="phone" class="input" value="<?php echo esc_attr( wp_unslash( $phone ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="hotel_name"><?php _e( 'Hotel Name', 'travelo' ) ?><br />
                <input type="text" name="hotel_name" id="hotel_name" class="input" value="<?php echo esc_attr( wp_unslash( $hotel_name ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="date_check"><?php _e( 'Date Check in', 'travelo' ) ?><br />
                <input type="date" name="date_check" id="date_check" class="input" value="<?php echo esc_attr( wp_unslash( $date_check ) ); ?>" size="25" /></label>
            </p>            
            <div class="clear"></div>

            <h3 class="info_header">EMERGENCY CONTACT DETAILS</h3>
            <p>
                <label for="emergency_name"><?php _e( 'Name', 'travelo' ) ?><br />
                <input type="text" name="emergency_name" id="emergency_name" class="input" value="<?php echo esc_attr( wp_unslash( $emergency_name ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="emergency_relationship"><?php _e( 'Relationship', 'travelo' ) ?><br />
                <input type="text" name="emergency_relationship" id="emergency_relationship" class="input" value="<?php echo esc_attr( wp_unslash( $emergency_relationship ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="emergency_mobile_no"><?php _e( 'Mobile Phone', 'travelo' ) ?><br />
                <input type="text" name="emergency_mobile_no" id="emergency_mobile_no" class="input" value="<?php echo esc_attr( wp_unslash( $emergency_mobile_no ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="emergency_home_phone"><?php _e( 'Home Phone', 'travelo' ) ?><br />
                <input type="text" name="emergency_home_phone" id="emergency_home_phone" class="input" value="<?php echo esc_attr( wp_unslash( $emergency_home_phone ) ); ?>" size="25" /></label>
            </p>            
            <div class="clear"></div>

            <h3 class="info_header">DIVING DETAILS</h3>
            <p>
                <label for="diving_certificate_agency"><?php _e( 'Certification Agency', 'travelo' ) ?><br />
                <input type="text" name="diving_certificate_agency" id="diving_certificate_agency" class="input" value="<?php echo esc_attr( wp_unslash( $diving_certificate_agency ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="diving_certification_level"><?php _e( 'Certification Level', 'travelo' ) ?><br />
                <input type="text" name="diving_certification_level" id="diving_certification_level" class="input" value="<?php echo esc_attr( wp_unslash( $diving_certification_level ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="number_of_dives"><?php _e( 'Number of Dives', 'travelo' ) ?><br />
                <input type="text" name="number_of_dives" id="number_of_dives" class="input" value="<?php echo esc_attr( wp_unslash( $number_of_dives ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="date_of_last_dive"><?php _e( 'Date of Last Dive', 'travelo' ) ?><br />
                <input type="date" name="date_of_last_dive" id="date_of_last_dive" class="input" value="<?php echo esc_attr( wp_unslash( $date_of_last_dive ) ); ?>" size="25" /></label>
            </p> 
            <p class="checkbox">
                <label for="prior_dsc"><?php _e( 'Prior DCS', 'travelo' ) ?></label>
                <input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="no" /><span>No</span>
            </p>            
            <div class="clear"></div>

            <h3 class="info_header">INSURANCE</h3>
            <p class="checkbox">
                <label for="have_dive_insurance"><?php _e( 'Do you have dive insurance?', 'travelo' ) ?></label><br>
                <input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="no" /><span>No</span>
            </p> 

            <p>
                <label for="dive_insurance_no"><?php _e( 'If yes please provide number', 'travelo' ) ?><br />
                <input type="text" name="dive_insurance_no" id="dive_insurance_no" class="input" value="<?php echo esc_attr( wp_unslash( $dive_insurance_no ) ); ?>" size="25" /></label>
            </p>
             
            <p class="checkbox">
                <label for="need_dive_insurance"><?php _e( 'Do you need dive insurance?', 'travelo' ) ?></label><br>
                <input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="no" /><span>No</span>
            </p>            
            <div class="clear"></div>
            <br>
            <h3 class="info_header">EQUIPMENT INFORMATION</h3>
            <p class="checkbox fullwidth">
                <label for="rent_diving_equipment"><?php _e( 'Do you want to rent diving equipment? ', 'travelo' ) ?></label><br>
                <input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="no" /><span>No</span>
            </p> 
            <h4>If “yes” please provide the following sizes:</h4>
            <p class="checkbox fullwidth">
                <label for="size_bcd"><?php _e( 'BCD: ', 'travelo' ) ?><br>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXS" /><span>XXS</span>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XS" /><span>XS</span>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="S" /><span>S</span>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="M" /><span>M</span>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="L" /><span>L</span>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XL" /><span>XL</span>
                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXL" /><span>XXL</span>
            </p>
             
            <p class="checkbox fullwidth">
                <label for="size_websuit"><?php _e( 'Wetsuit: ', 'travelo' ) ?></label><br>
                <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XS" /><span>XS</span>
                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="S" /><span>S</span>
                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="M" /><span>M</span>
                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="L" /><span>L</span>
                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XL" /><span>XL</span>
                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XXL" /><span>XXL</span>
            </p> 
            <p class="checkbox fullwidth">
                <label for="size_fins"><?php _e( 'Fins: ', 'travelo' ) ?></label><br>
                <input type="radio" name="size_fins" id="size_fins" class="input" value="36-37" /><span>36-37</span>
                 <input type="radio" name="size_fins" id="size_fins" class="input" value="38-39" /><span>38-39</span>
                 <input type="radio" name="size_fins" id="size_fins" class="input" value="40-41" /><span>40-41</span>
                 <input type="radio" name="size_fins" id="size_fins" class="input" value="42-43" /><span>42-43</span>
                 <input type="radio" name="size_fins" id="size_fins" class="input" value="44-45" /><span>44-45</span>
            </p>
            <p class="checkbox">
                <label for="regulator"><?php _e( 'Regulator ', 'travelo' ) ?></label><br>
                <input type="radio" name="regulator" id="regulator" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="regulator" id="regulator" class="input" value="no" /><span>No</span>
            </p>
            <p class="checkbox">
                <label for="mask"><?php _e( 'Mask', 'travelo' ) ?></label><br>
                <input type="radio" name="mask" id="mask" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="mask" id="mask" class="input" value="no" /><span>No</span>
            </p>
            <h4>*If you are unsure about sizing please state your:</h4> 
            <p>
                <label for="weight"><?php _e( 'Weight', 'travelo' ) ?><br />
                <input type="text" name="weight" id="Weight" class="input" value="<?php echo esc_attr( wp_unslash( $weight ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="height"><?php _e( 'Height', 'travelo' ) ?><br />
                <input type="text" name="height" id="height" class="input" value="<?php echo esc_attr( wp_unslash( $height ) ); ?>" size="25" /></label>
            </p>
            <p>
                <label for="shoe_size"><?php _e( 'Shoe Size (EU)', 'travelo' ) ?><br />
                <input type="text" name="shoe_size" id="shoe_size" class="input" value="<?php echo esc_attr( wp_unslash( $shoe_size ) ); ?>" size="25" /></label>
            </p>              
            <p class="checkbox">
                <label for="need_din"><?php _e( 'Do you need a DIN? ', 'travelo' ) ?></label><br>
                <input type="radio" name="need_din" id="need_din" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="need_din" id="need_din" class="input" value="no" /><span>No</span>
            </p>
            <p class="checkbox fullwidth">
                <label for="rent_dive_computer"><?php _e( 'Do you want to rent a Dive Computer? ', 'travelo' ) ?></label><br>
                <input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="yes" /><span>Yes</span>
                <input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="no" /><span>No</span>
            </p>

            <p class="checkbox fullwidth"> 
            
                <label for="optional_course"><?php _e( 'Courses (optional) ', 'travelo' ) ?></label>  <br>               
                <input type="radio" name="optional_course" id="optional_course" class="input" value="DSD" /><span>DSD (Discover Scuba Diving)</span>
                <input type="radio" name="optional_course" id="optional_course" class="input" value="Refresher" /><span>Refresher</span>
                <input type="radio" name="optional_course" id="optional_course" class="input" value="Open Water Course" /><span>Open Water Course</span> <br>
                <input type="radio" name="optional_course" id="optional_course" class="input" value="Advanced Open Water Course" /><span>Advanced Open Water Course</span>
                <input type="radio" name="optional_course" id="optional_course" class="input" value="Adventure Deep Dive" /><span>Adventure Deep Dive</span>
                <input type="radio" name="optional_course" id="optional_course" class="input" value="Specialty" /><span>Specialty</span>
        
            </p>      
            <div class="clear"></div> 

        <?php
    }

   //2. Add validation. validation section
    add_filter( 'registration_errors', 'travelo_registration_errors', 10, 3 );
    function travelo_registration_errors( $errors, $sanitized_user_login, $user_email ) {
        
        if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
            $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'travelo' ) );
        }
        if(($_POST['have_dive_insurance'] =='yes') && empty($_POST['dive_insurance_no'])){

            $errors->add( 'dive_insurance', __( '<strong>ERROR</strong>: You must include a dive insurance no.', 'travelo' ) );
        }
        if(($_POST['rent_diving_equipment'] =='yes') && (empty( $_POST['size_bcd'] )|| empty( $_POST['size_websuit'] ) || empty( $_POST['size_fins'] )) ){

            $errors->add( 'dive_equipment', __( '<strong>ERROR</strong>: You must include sizes.', 'travelo' ) );
        }
        /*if ( !preg_match('/^[0-9]+$/', $_POST['vat_no']) || empty( $_POST['vat_no'] ) || ! empty( $_POST['vat_no'] ) && trim( $_POST['vat_no'] ) == '' ) {
            $errors->add( 'vat_no_error', __( '<strong>ERROR</strong>: You must include a valid vat no.', 'mydomain' ) );
        }*/
        

        if ( $_POST['password'] !== $_POST['repeat_password'] ) {
            $errors->add( 'passwords_not_matched', "<strong>ERROR</strong>: Passwords must match" );
        }
        if ( strlen( $_POST['password'] ) < 5 ) {
            $errors->add( 'password_too_short', "<strong>ERROR</strong>: Passwords must be at least five characters long" );
        }   

        return $errors;
    }

    //3.save our extra registration user meta.
    add_action( 'user_register', 'travelo_user_register' );
    function travelo_user_register( $user_id ) {
        $userdata = array();
        $userdata['ID'] = $user_id;
        if ( $_POST['password'] !== '' ) {
            $userdata['user_pass'] = $_POST['password'];
        }
        $new_user_id = wp_update_user( $userdata );
        /*Personal Information*/
        if ( ! empty( $_POST['first_name'] ) ) {
            update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
        }
        if ( ! empty( $_POST['middle_initial'] ) ) {
            update_user_meta( $user_id, 'middle_initial', trim( $_POST['middle_initial'] ) );
        }
        if ( ! empty( $_POST['last_name'] ) ) {
            update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
        }
        if ( ! empty( $_POST['gender'] ) ) {
            update_user_meta( $user_id, 'gender', trim( $_POST['gender'] ) );
        }
        if ( ! empty( $_POST['nationality'] ) ) {
            update_user_meta( $user_id, 'nationality', trim( $_POST['nationality'] ) );
        }
        if ( ! empty( $_POST['date_of_birth'] ) ) {
            update_user_meta( $user_id, 'date_of_birth', trim( $_POST['date_of_birth'] ) );
        }
        if ( ! empty( $_POST['passport_number'] ) ) {
            update_user_meta( $user_id, 'passport_number', trim( $_POST['passport_number'] ) );
        }
        if ( ! empty( $_POST['dietary_req'] ) ) {
            update_user_meta( $user_id, 'dietary_req', trim( $_POST['dietary_req'] ) );
        }
        if ( ! empty( $_POST['allergies'] ) ) {
            update_user_meta( $user_id, 'allergies', trim( $_POST['allergies'] ) );
        }
        /*CONTACT DETAILS*/
        if ( ! empty( $_POST['home_address'] ) ) {
            update_user_meta( $user_id, 'home_address', trim( $_POST['home_address'] ) );
        }
        if ( ! empty( $_POST['phone'] ) ) {
            update_user_meta( $user_id, 'phone', trim( $_POST['phone'] ) );
        }
        if ( ! empty( $_POST['hotel_name'] ) ) {
            update_user_meta( $user_id, 'hotel_name', trim( $_POST['hotel_name'] ) );
        }
        if ( ! empty( $_POST['date_check'] ) ) {
            update_user_meta( $user_id, 'date_check', trim( $_POST['date_check'] ) );
        }
        /*EMERGENCY CONTACT DETAILS*/
        if ( ! empty( $_POST['emergency_name'] ) ) {
            update_user_meta( $user_id, 'emergency_name', trim( $_POST['emergency_name'] ) );
        }
        if ( ! empty( $_POST['emergency_relationship'] ) ) {
            update_user_meta( $user_id, 'emergency_relationship', trim( $_POST['emergency_relationship'] ) );
        }
        if ( ! empty( $_POST['emergency_mobile_no'] ) ) {
            update_user_meta( $user_id, 'emergency_mobile_no', trim( $_POST['emergency_mobile_no'] ) );
        }
        if ( ! empty( $_POST['emergency_home_phone'] ) ) {
            update_user_meta( $user_id, 'emergency_home_phone', trim( $_POST['emergency_home_phone'] ) );
        }
        

        /*DIVING DETAILS*/
        if ( ! empty( $_POST['diving_certificate_agency'] ) ) {
            update_user_meta( $user_id, 'diving_certificate_agency', trim( $_POST['diving_certificate_agency'] ) );
        }
        if ( ! empty( $_POST['diving_certification_level'] ) ) {
            update_user_meta( $user_id, 'diving_certification_level', trim( $_POST['diving_certification_level'] ) );
        }
        if ( ! empty( $_POST['number_of_dives'] ) ) {
            update_user_meta( $user_id, 'number_of_dives', trim( $_POST['number_of_dives'] ) );
        }
        if ( ! empty( $_POST['date_of_last_dive'] ) ) {
            update_user_meta( $user_id, 'date_of_last_dive', trim( $_POST['date_of_last_dive'] ) );
        }
        if ( ! empty( $_POST['prior_dsc'] ) ) {
            update_user_meta( $user_id, 'prior_dsc', trim( $_POST['prior_dsc'] ) );
        }
        /*Insurance*/
        if ( ! empty( $_POST['have_dive_insurance'] ) ) {
            update_user_meta( $user_id, 'have_dive_insurance', trim( $_POST['have_dive_insurance'] ) );
        }
        if ( ! empty( $_POST['dive_insurance_no'] ) ) {
            update_user_meta( $user_id, 'dive_insurance_no', trim( $_POST['dive_insurance_no'] ) );
        }
        if ( ! empty( $_POST['need_dive_insurance'] ) ) {
            update_user_meta( $user_id, 'need_dive_insurance', trim( $_POST['need_dive_insurance'] ) );
        }

        /*EQUIPMENT INFORMATION*/
         if ( ! empty( $_POST['rent_diving_equipment'] ) ) {
            update_user_meta( $user_id, 'rent_diving_equipment', trim( $_POST['rent_diving_equipment'] ) );
        }
        if ( ! empty( $_POST['size_bcd'] ) ) {
            update_user_meta( $user_id, 'size_bcd', trim( $_POST['size_bcd'] ) );
        }
        if ( ! empty( $_POST['size_websuit'] ) ) {
            update_user_meta( $user_id, 'size_websuit', trim( $_POST['size_websuit'] ) );
        }
         if ( ! empty( $_POST['size_fins'] ) ) {
            update_user_meta( $user_id, 'size_fins', trim( $_POST['size_fins'] ) );
        }
        if ( ! empty( $_POST['regulator'] ) ) {
            update_user_meta( $user_id, 'regulator', trim( $_POST['regulator'] ) );
        }
        if ( ! empty( $_POST['mask'] ) ) {
            update_user_meta( $user_id, 'mask', trim( $_POST['mask'] ) );
        }
         if ( ! empty( $_POST['weight'] ) ) {
            update_user_meta( $user_id, 'weight', trim( $_POST['weight'] ) );
        }
        if ( ! empty( $_POST['height'] ) ) {
            update_user_meta( $user_id, 'height', trim( $_POST['height'] ) );
        }
        if ( ! empty( $_POST['shoe_size'] ) ) {
            update_user_meta( $user_id, 'shoe_size', trim( $_POST['shoe_size'] ) );
        }
         if ( ! empty( $_POST['need_din'] ) ) {
            update_user_meta( $user_id, 'need_din', trim( $_POST['need_din'] ) );
        }
        if ( ! empty( $_POST['rent_dive_computer'] ) ) {
            update_user_meta( $user_id, 'rent_dive_computer', trim( $_POST['rent_dive_computer'] ) );
        }
        if ( ! empty( $_POST['optional_course'] ) ) {
            update_user_meta( $user_id, 'optional_course', trim( $_POST['optional_course'] ) );
        }

        /*if ( ! empty( $_POST['vat_no'] ) ) {
            update_user_meta( $user_id, 'vat_no', trim( $_POST['vat_no'] ) );
        }*/
    }

 ?>