<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$user_info = trav_get_current_user_info();
$_countries = trav_get_all_countries();
global $trav_options;


do_action( 'trav_booking_form_before' ); 
?>

<div class="person-information">
	<h2><?php _e( 'Your Personal Information', 'trav'); ?></h2>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'first name', 'trav'); ?></label>
			<input required type="text" name="first_name" class="input-text full-width" value="<?php echo esc_attr( $user_info['first_name'] ) ?>" placeholder="" />
		</div>
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'last name', 'trav'); ?></label>
			<input required type="text" name="last_name" class="input-text full-width" value="<?php echo esc_attr( $user_info['last_name'] ) ?>" placeholder="" />
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'email address', 'trav'); ?></label>
			<input required type="text" name="email" class="input-text full-width" value="<?php echo esc_attr( $user_info['email'] ) ?>" placeholder="" />
		</div>
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Verify E-mail Address', 'trav'); ?></label>
			<input required type="text" name="email2" class="input-text full-width" value="<?php echo esc_attr( $user_info['email'] ) ?>" placeholder="" />
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Country code', 'trav'); ?></label>
			<div class="selector">
				<select class="full-width" name="country_code">
					<?php foreach ( $_countries as $_country ) { ?>
						<option value="<?php echo esc_attr( $_country['d_code'] ) ?>" <?php selected( $user_info['country_code'], $_country['name'] . ' (' . $_country['d_code'] . ')' ); ?>><?php echo esc_html( $_country['name'] . ' (' . $_country['d_code'] . ')' ) ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Phone number', 'trav'); ?></label>
			<input type="text" name="phone" class="input-text full-width" value="<?php echo esc_attr( $user_info['phone'] ) ?>" placeholder="" />
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Certification Level', 'trav'); ?></label>
			<input required type="text" name="certification_level" class="input-text full-width" value="<?php echo esc_attr( $user_info['certification_level'] ) ?>" placeholder="" />
		</div>		
	</div>
	<div class="form-group row">
		<div class="col-sm-12 col-md-10">
			<label><?php _e( 'Comments', 'trav'); ?></label>
			<textarea required name="special_requirements" class="full-width" rows="4"></textarea>
		</div>
	</div>

	<!-- TRIP INFORMATION -->
	<h2><?php _e( 'Trip Information', 'trav'); ?></h2>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Destination', 'trav'); ?></label>
			<input required type="text" name="trip_destination" class="input-text full-width" value="<?php echo esc_attr( $user_info['trip_destination'] ) ?>" placeholder="" />
		</div>
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Boat Name', 'trav'); ?></label>
			<input type="text" name="trip_boat_name" class="input-text full-width" value="<?php echo esc_attr( $user_info['trip_boat_name'] ) ?>" placeholder="" />
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Trip Date', 'trav'); ?></label>
			<input required type="date" name="trip_date" class="input-text full-width" value="<?php echo esc_attr( $user_info['trip_date'] ) ?>" placeholder="" />
		</div>
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Trip Duration', 'trav'); ?></label>
			<input type="text" name="trip_duration" class="input-text full-width" value="<?php echo esc_attr( $user_info['trip_duration'] ) ?>" placeholder="" />
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5">
			<label><?php _e( 'Number of People', 'trav'); ?></label>
			<input required type="text" name="number_of_people" class="input-text full-width" value="<?php echo esc_attr( $user_info['number_of_people'] ) ?>" placeholder="" />
		</div>
		<div class="col-sm-6 col-md-5 prefer_loacation">
			<label><?php _e( 'Cabin preferences', 'trav'); ?></label>
			<input type="radio" name="cabin_preferences" id="cabin_preferences" class="input" value="Ensuite" /><span>Ensuite</span>
            <input type="radio" name="cabin_preferences" id="cabin_preferences" class="input" value="Shared WC" /><span>Shared WC</span>
		</div>
		
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5 prefer_loacation">
			<label><?php _e( 'Bed Configuration', 'trav'); ?></label>
			<input type="radio" name="bed_configaration" id="bed_configaration" class="input" value="Single" /><span>Single</span>
            <input type="radio" name="bed_configaration" id="bed_configaration" class="input" value="Twin" /><span>Twin</span>
            <input type="radio" name="bed_configaration" id="bed_configaration" class="input" value="Double" /><span>Double</span>
            <input type="radio" name="bed_configaration" id="bed_configaration" class="input" value="Triple" /><span>Triple</span>
            <input type="radio" name="bed_configaration" id="bed_configaration" class="input" value="Quad" /><span>Quad</span>
		</div>
		<div class="col-sm-6 col-md-5 prefer_loacation">
			<label><?php _e( 'Quality/Price Range', 'trav'); ?></label>
			<input type="radio" name="price_range" id="price_range" class="input" value="Budget" /><span>Budget</span>
            <input type="radio" name="price_range" id="price_range" class="input" value="Mid-range" /><span>Mid-range</span>
            <input type="radio" name="price_range" id="price_range" class="input" value="Master" /><span>Master</span>
		</div>
		
	</div>
	<div class="form-group row">
		<div class="col-sm-6 col-md-5 prefer_loacation">
			<label><?php _e( 'Location', 'trav'); ?></label>
			<input type="radio" name="deck_location" id="deck_location" class="input" value="Upper Deck" /><span>Upper Deck</span>
            <input type="radio" name="deck_location" id="deck_location" class="input" value="Main Deck" /><span>Main Deck</span>
            <input type="radio" name="deck_location" id="deck_location" class="input" value="Lower Deck" /><span>Lower Deck</span>            
		</div>
	</div>

</div>
<hr />

<?php do_action( 'trav_booking_form_after' ); ?>

<div class="form-group row confirm-booking-btn">
	<div class="col-sm-6 col-md-5">
		<button type="submit" class="full-width btn-large" name="confirm_booking">
			<?php $button_text = __( 'ENQUIRE NOW', 'trav'); ?>
			<?php echo apply_filters( 'trav_booking_button_text', $button_text ); ?>
		</button>
	</div>
</div>