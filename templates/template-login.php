<?php
 /*
 Template Name: Login Page Template
 */
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="ie7 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE   ]>    <html class="ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html <?php language_attributes(); ?>>
<head>
	<!-- Page Title -->
	<title><?php wp_title(' - ', true, 'right'); ?></title>

	<!-- Meta Tags -->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php global $trav_options, $logo_url, $redirect_url_on_login, $login_url, $signup_url; ?>
	<?php
		$login_url = strtok($_SERVER["REQUEST_URI"],'?');
		$signup_url = add_query_arg( 'action', 'register', trav_get_permalink_clang( $trav_options['login_page'] ) );
	?>
	<?php if ( ! empty( $trav_options['favicon'] ) && ! empty( $trav_options['favicon']['url'] ) ): ?>
	<link rel="shortcut icon" href="<?php echo esc_url( $trav_options['favicon']['url'] ); ?>" type="image/x-icon" />
	<?php endif; ?>

	<!-- Theme Styles -->
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,200,300,500' rel='stylesheet' type='text/css'>

	<!-- CSS for IE -->
	<!--[if lte IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css" />
	<![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	  <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
	<![endif]-->
	<?php wp_head();?>
</head>
<body <?php body_class( array( 'soap-login-page', 'style1', 'body-blank' ) ); ?>>
<?php
	if ( have_posts() ) {
		while ( have_posts() ) : the_post();
?>
	<div id="page-wrapper" class="wrapper-blank">
		<header id="header" class="navbar-static-top">
			<a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle blue-bg">Mobile Menu Toggle</a>
			<div class="container"><h1 class="logo"></h1></div>
			<!-- mobile menu -->
			<?php if ( has_nav_menu( 'header-menu' ) ) {
					wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav', 'container_class' => 'mobile-menu collapse', 'container_id' => 'mobile-menu-01', 'menu_class' => 'menu', 'menu_id' => 'mobile-primary-menu' ) ); 
				} else { ?>
					<nav id="mobile-menu-01" class="mobile-menu collapse">
						<ul id="mobile-primary-menu" class="wrap">
							<li class="menu-item"><a href="<?php echo esc_url( home_url() ); ?>"><?php _e('Home', "trav"); ?></a></li>
							<li class="menu-item"><a href="<?php echo esc_url( admin_url('nav-menus.php') ); ?>"><?php _e('Configure', "trav"); ?></a></li>
						</ul>
					</nav>
			<?php } ?>
			<!-- mobile menu -->
		</header>
		<section id="content">
			<div class="container">
				<div id="main">
					<h1 class="logo block">
						<a href="<?php echo esc_url( home_url() ); ?>" title="Travelo - home">
							<img src="<?php echo esc_url( $logo_url ); ?>" alt="<?php bloginfo('name'); ?>" />
						</a>
					</h1>
					<div class="text-center yellow-color box" style="font-size: 4em; font-weight: 300; line-height: 1em;"><?php echo empty( $trav_options['welcome_txt'] ) ? __( 'Welcome to Travelo!', 'trav' ) : __( $trav_options['welcome_txt'], 'trav' ) ?></div>
					<p class="light-blue-color block" style="font-size: 1.3333em;">
						<?php if ( isset( $_GET['action'] ) && ( $_GET['action'] == 'register' ) ) {
								_e( 'Register For This Site', 'trav' );
							} else if ( isset( $_GET['action'] ) && ( $_GET['action'] == 'lostpassword' ) ) {
								echo __( 'Please enter your username or email address.', 'trav') . '<br />' . __( 'You will receive a link to create a new password via email.', 'trav' );
							} else if ( isset( $_GET['checkemail'] ) ) {
								_e( 'Check your e-mail for the confirmation link.', 'trav' );
							} else {
								_e( 'Please login to your account.', 'trav' );
							}
						?>
					</p>
					<?php the_content(); ?>
					<div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">
						<?php if ( isset( $_GET['action'] ) && ( $_GET['action'] == 'register' ) ) { 
							/*PERSONAL INFORMATION*/            
					        if(isset($_POST['first_name'])){
					            $first_name = trim($_POST['first_name']);
					        }       
					        if(isset($_POST['middle_initial'])){
					            $middle_initial = trim($_POST['middle_initial']);
					        }        
					        if(isset($_POST['last_name'])){
					            $last_name = trim($_POST['last_name']);
					        }
					        if(isset($_POST['gender'])){
					            $gender = trim($_POST['gender']);
					        }
					        if(isset($_POST['nationality'])){
					            $nationality = trim($_POST['nationality']);
					        }
					        if(isset($_POST['birthday'])){
					            $birthday = trim($_POST['birthday']);
					        }
					        if(isset($_POST['passport_number'])){
					            $passport_number = trim($_POST['passport_number']);
					        }
					        if(isset($_POST['dietary_req'])){
					            $dietary_req = trim($_POST['dietary_req']);
					        }
					        if(isset($_POST['allergies'])){
					            $allergies = trim($_POST['allergies']);
					        }

					        /*CONTACT DETAILS*/
					        if(isset($_POST['home_address'])){
					            $home_address = trim($_POST['home_address']);
					        }
					        if(isset($_POST['phone'])){
					            $phone = trim($_POST['phone']);
					        }
					        if(isset($_POST['hotel_name'])){
					            $hotel_name = trim($_POST['hotel_name']);
					        }
					        if(isset($_POST['date_check'])){
					            $date_check = trim($_POST['date_check']);
					        }

					        /*EMERGENCY CONTACT DETAILS*/
					        if(isset($_POST['emergency_name'])){
					            $emergency_name = trim($_POST['emergency_name']);
					        }
					        if(isset($_POST['emergency_relationship'])){
					            $emergency_relationship = trim($_POST['emergency_relationship']);
					        }
					        if(isset($_POST['emergency_mobile_no'])){
					            $emergency_mobile_no = trim($_POST['emergency_mobile_no']);
					        }
					        if(isset($_POST['emergency_home_phone'])){
					            $emergency_home_phone = trim($_POST['emergency_home_phone']);
					        }

					        /*DIVING DETAILS*/
					        if(isset($_POST['diving_certificate_agency'])){
					            $diving_certificate_agency = trim($_POST['diving_certificate_agency']);
					        }
					        if(isset($_POST['diving_certification_level'])){
					            $diving_certification_level = trim($_POST['diving_certification_level']);
					        }
					        if(isset($_POST['number_of_dives'])){
					            $number_of_dives = trim($_POST['number_of_dives']);
					        }
					        if(isset($_POST['date_of_last_dive'])){
					            $date_of_last_dive = trim($_POST['date_of_last_dive']);
					        }
					        if(isset($_POST['prior_dsc'])){
					            $prior_dsc = trim($_POST['prior_dsc']);
					        }

					        /*Insurance*/       
					        if(isset($_POST['have_dive_insurance'])){
					            $have_dive_insurance = trim($_POST['have_dive_insurance']);
					        }
					        if(isset($_POST['dive_insurance_no'])){
					            $dive_insurance_no = trim($_POST['dive_insurance_no']);
					        }
					        if(isset($_POST['need_dive_insurance'])){
					            $need_dive_insurance = trim($_POST['need_dive_insurance']);
					        }   

					        /*EQUIPMENT INFORMATION*/   
					        if(isset($_POST['rent_diving_equipment'])){
					            $rent_diving_equipment = trim($_POST['rent_diving_equipment']);
					        }
					        if(isset($_POST['size_bcd'])){
					            $size_bcd = trim($_POST['size_bcd']);
					        }
					        if(isset($_POST['size_websuit'])){
					            $size_websuit = trim($_POST['size_websuit']);
					        }
					        if(isset($_POST['size_fins'])){
					            $size_fins = trim($_POST['size_fins']);
					        }
					        if(isset($_POST['regulator'])){
					            $regulator = trim($_POST['regulator']);
					        }       
					        if(isset($_POST['mask'])){
					            $mask = trim($_POST['mask']);
					        }
					        if(isset($_POST['weight'])){
					            $weight = trim($_POST['weight']);
					        }
					        if(isset($_POST['height'])){
					            $height = trim($_POST['height']);
					        }
					        if(isset($_POST['shoe_size'])){
					            $shoe_size = trim($_POST['shoe_size']);
					        }
					        if(isset($_POST['need_din'])){
					            $need_din = trim($_POST['need_din']);
					        }
					        if(isset($_POST['rent_dive_computer'])){
					            $rent_dive_computer = trim($_POST['rent_dive_computer']);
					        }
					        if(isset($_POST['optional_course'])){
					            $optional_course = trim($_POST['optional_course']);
					        }

					        $userdata = array();
						        $userdata['ID'] = $user_id;
						        if ( $_POST['password'] !== '' ) {
						            $userdata['user_pass'] = $_POST['password'];
						        }
						        $new_user_id = wp_update_user( $userdata );
						        /*Personal Information*/
						        if ( ! empty( $_POST['first_name'] ) ) {
						            update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
						        }
						        if ( ! empty( $_POST['middle_initial'] ) ) {
						            update_user_meta( $user_id, 'middle_initial', trim( $_POST['middle_initial'] ) );
						        }
						        if ( ! empty( $_POST['last_name'] ) ) {
						            update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
						        }
						        if ( ! empty( $_POST['gender'] ) ) {
						            update_user_meta( $user_id, 'gender', trim( $_POST['gender'] ) );
						        }
						        if ( ! empty( $_POST['nationality'] ) ) {
						            update_user_meta( $user_id, 'nationality', trim( $_POST['nationality'] ) );
						        }
						        if ( ! empty( $_POST['date_of_birth'] ) ) {
						            update_user_meta( $user_id, 'date_of_birth', trim( $_POST['date_of_birth'] ) );
						        }
						        if ( ! empty( $_POST['passport_number'] ) ) {
						            update_user_meta( $user_id, 'passport_number', trim( $_POST['passport_number'] ) );
						        }
						        if ( ! empty( $_POST['dietary_req'] ) ) {
						            update_user_meta( $user_id, 'dietary_req', trim( $_POST['dietary_req'] ) );
						        }
						        if ( ! empty( $_POST['allergies'] ) ) {
						            update_user_meta( $user_id, 'allergies', trim( $_POST['allergies'] ) );
						        }
						        /*CONTACT DETAILS*/
						        if ( ! empty( $_POST['home_address'] ) ) {
						            update_user_meta( $user_id, 'home_address', trim( $_POST['home_address'] ) );
						        }
						        if ( ! empty( $_POST['phone'] ) ) {
						            update_user_meta( $user_id, 'phone', trim( $_POST['phone'] ) );
						        }
						        if ( ! empty( $_POST['hotel_name'] ) ) {
						            update_user_meta( $user_id, 'hotel_name', trim( $_POST['hotel_name'] ) );
						        }
						        if ( ! empty( $_POST['date_check'] ) ) {
						            update_user_meta( $user_id, 'date_check', trim( $_POST['date_check'] ) );
						        }
						        /*EMERGENCY CONTACT DETAILS*/
						        if ( ! empty( $_POST['emergency_name'] ) ) {
						            update_user_meta( $user_id, 'emergency_name', trim( $_POST['emergency_name'] ) );
						        }
						        if ( ! empty( $_POST['emergency_relationship'] ) ) {
						            update_user_meta( $user_id, 'emergency_relationship', trim( $_POST['emergency_relationship'] ) );
						        }
						        if ( ! empty( $_POST['emergency_mobile_no'] ) ) {
						            update_user_meta( $user_id, 'emergency_mobile_no', trim( $_POST['emergency_mobile_no'] ) );
						        }
						        if ( ! empty( $_POST['emergency_home_phone'] ) ) {
						            update_user_meta( $user_id, 'emergency_home_phone', trim( $_POST['emergency_home_phone'] ) );
						        }
						        

						        /*DIVING DETAILS*/
						        if ( ! empty( $_POST['diving_certificate_agency'] ) ) {
						            update_user_meta( $user_id, 'diving_certificate_agency', trim( $_POST['diving_certificate_agency'] ) );
						        }
						        if ( ! empty( $_POST['diving_certification_level'] ) ) {
						            update_user_meta( $user_id, 'diving_certification_level', trim( $_POST['diving_certification_level'] ) );
						        }
						        if ( ! empty( $_POST['number_of_dives'] ) ) {
						            update_user_meta( $user_id, 'number_of_dives', trim( $_POST['number_of_dives'] ) );
						        }
						        if ( ! empty( $_POST['date_of_last_dive'] ) ) {
						            update_user_meta( $user_id, 'date_of_last_dive', trim( $_POST['date_of_last_dive'] ) );
						        }
						        if ( ! empty( $_POST['prior_dsc'] ) ) {
						            update_user_meta( $user_id, 'prior_dsc', trim( $_POST['prior_dsc'] ) );
						        }
						        /*Insurance*/
						        if ( ! empty( $_POST['have_dive_insurance'] ) ) {
						            update_user_meta( $user_id, 'have_dive_insurance', trim( $_POST['have_dive_insurance'] ) );
						        }
						        if ( ! empty( $_POST['dive_insurance_no'] ) ) {
						            update_user_meta( $user_id, 'dive_insurance_no', trim( $_POST['dive_insurance_no'] ) );
						        }
						        if ( ! empty( $_POST['need_dive_insurance'] ) ) {
						            update_user_meta( $user_id, 'need_dive_insurance', trim( $_POST['need_dive_insurance'] ) );
						        }

						        /*EQUIPMENT INFORMATION*/
						         if ( ! empty( $_POST['rent_diving_equipment'] ) ) {
						            update_user_meta( $user_id, 'rent_diving_equipment', trim( $_POST['rent_diving_equipment'] ) );
						        }
						        if ( ! empty( $_POST['size_bcd'] ) ) {
						            update_user_meta( $user_id, 'size_bcd', trim( $_POST['size_bcd'] ) );
						        }
						        if ( ! empty( $_POST['size_websuit'] ) ) {
						            update_user_meta( $user_id, 'size_websuit', trim( $_POST['size_websuit'] ) );
						        }
						         if ( ! empty( $_POST['size_fins'] ) ) {
						            update_user_meta( $user_id, 'size_fins', trim( $_POST['size_fins'] ) );
						        }
						        if ( ! empty( $_POST['regulator'] ) ) {
						            update_user_meta( $user_id, 'regulator', trim( $_POST['regulator'] ) );
						        }
						        if ( ! empty( $_POST['mask'] ) ) {
						            update_user_meta( $user_id, 'mask', trim( $_POST['mask'] ) );
						        }
						         if ( ! empty( $_POST['weight'] ) ) {
						            update_user_meta( $user_id, 'weight', trim( $_POST['weight'] ) );
						        }
						        if ( ! empty( $_POST['height'] ) ) {
						            update_user_meta( $user_id, 'height', trim( $_POST['height'] ) );
						        }
						        if ( ! empty( $_POST['shoe_size'] ) ) {
						            update_user_meta( $user_id, 'shoe_size', trim( $_POST['shoe_size'] ) );
						        }
						         if ( ! empty( $_POST['need_din'] ) ) {
						            update_user_meta( $user_id, 'need_din', trim( $_POST['need_din'] ) );
						        }
						        if ( ! empty( $_POST['rent_dive_computer'] ) ) {
						            update_user_meta( $user_id, 'rent_dive_computer', trim( $_POST['rent_dive_computer'] ) );
						        }
						        if ( ! empty( $_POST['optional_course'] ) ) {
						            update_user_meta( $user_id, 'optional_course', trim( $_POST['optional_course'] ) );
						        }
							
							?>
							<form name="registerform" class="login-form login" action="<?php echo esc_url( wp_registration_url() )?>" method="post">
								<p id="reg_passmail"><?php _e( 'A password will be e-mailed to you.', 'trav') ?></p>
								<div class="form-group">
									<input type="text" name="user_login"  class="input-text input-large full-width" placeholder="<?php _e( 'User Name', 'trav' ); ?>" value="" size="20"> 
								</div>

								<div class="form-group">
									<input type="text" name="user_email"  class="input-text input-large full-width" placeholder="<?php _e( 'Email', 'trav' ); ?>" value="" size="25"> 
								</div>

								<div class="form-group">
									<input id="password" class="input-text input-large full-width" type="password" tabindex="30" size="25" placeholder="<?php _e( 'Password', 'trav' ); ?>" value="" name="password" />
								</div>

								<div class="form-group">
									<input id="repeat_password" class="input-text input-large full-width" type="password" tabindex="30" size="25" placeholder="<?php _e( 'Repeat password', 'trav' ); ?>" value="" name="repeat_password" />
								</div> 

					            <h3 class="info_header">PERSONAL INFORMATION</h3>
					            
					            <div class="form-group">
									<input type="text" name="first_name"  class="input-text input-large full-width" placeholder="<?php _e( 'First Name', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25"> 
								</div>

								<div class="form-group">
									<input type="text" name="middle_initial" id="middle_initial" class="input-text input-large full-width" placeholder="<?php _e( 'Middle Initial', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $middle_initial ) ); ?>" size="25" />
								</div>

								<div class="form-group">
									<input type="text" name="last_name" id="last_name" class="input-text input-large full-width" placeholder="<?php _e( 'Last Name', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="25" />
								</div>	

					            <p>					                
					                <input type="text" name="gender" id="gender" class="input-text input-large full-width" placeholder="<?php _e( 'Gender', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $gender ) ); ?>" size="25" /> 
					            </p>

					            <p> 
					            	<input type="text" name="nationality" id="nationality" class="input-text input-large full-width" placeholder="<?php _e( 'Nationality', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $nationality ) ); ?>" size="25" /> 
					            </p>

					            <p> 
					                <input type="date" name="date_of_birth" id="date_of_birth" class="input-text input-large full-width" placeholder="<?php _e( 'Date of Birth', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $date_of_birth ) ); ?>" size="25" /> 
					            </p>

					            <p> 
					                <input type="text" name="passport_number" id="passport_number" class="input-text input-large full-width" placeholder="<?php _e( 'Passport Number', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $passport_number ) ); ?>" size="25" /> 
					            </p>

					            <p> 
					                <input type="text" name="dietary_req" id="dietary_req" class="input-text input-large full-width" placeholder="<?php _e( 'Dietary Requirements', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $dietary_req ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="allergies" id="allergies" class="input-text input-large full-width" placeholder="<?php _e( 'Allergies', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $allergies ) ); ?>" size="25" /> 
					            </p>

					            <div class="clear"></div>

					            <h3 class="info_header">CONTACT DETAILS</h3>
					            <p> 
					                <input type="text" name="home_address" id="home_address" class="input-text input-large full-width" placeholder="<?php _e( 'Home Address', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $home_address ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="phone" id="phone" class="input-text input-large full-width" placeholder="<?php _e( 'Mobile Phone', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $phone ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="hotel_name" id="hotel_name" class="input-text input-large full-width" placeholder="<?php _e( 'Hotel Name', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $hotel_name ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="date" name="date_check" id="date_check" class="input-text input-large full-width" placeholder="<?php _e( 'Date Check in', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $date_check ) ); ?>" size="25" /> 
					            </p>            
					            <div class="clear"></div>

					            <h3 class="info_header">EMERGENCY CONTACT DETAILS</h3>
					            <p> 
					                <input type="text" name="emergency_name" id="emergency_name" class="input-text input-large full-width" placeholder="<?php _e( 'Name', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $emergency_name ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="emergency_relationship" id="emergency_relationship" class="input-text input-large full-width" placeholder="<?php _e( 'Relationship', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $emergency_relationship ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="emergency_mobile_no" id="emergency_mobile_no" class="input-text input-large full-width" placeholder="<?php _e( 'Mobile Phone', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $emergency_mobile_no ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="emergency_home_phone" id="emergency_home_phone" class="input-text input-large full-width" placeholder="<?php _e( 'Home Phone', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $emergency_home_phone ) ); ?>" size="25" /> 
					            </p>            
					            <div class="clear"></div>

					            <h3 class="info_header">DIVING DETAILS</h3>
					            <p> 
					                <input type="text" name="diving_certificate_agency" id="diving_certificate_agency" class="input-text input-large full-width" placeholder="<?php _e( 'Certification Agency', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $diving_certificate_agency ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="diving_certification_level" id="diving_certification_level" class="input-text input-large full-width" placeholder="<?php _e( 'Certification Level', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $diving_certification_level ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="number_of_dives" id="number_of_dives" class="input-text input-large full-width" placeholder="<?php _e( 'Number of Dives', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $number_of_dives ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="date" name="date_of_last_dive" id="date_of_last_dive" class="input-text input-large full-width"  placeholder="<?php _e( 'Date of Last Dive', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $date_of_last_dive ) ); ?>" size="25" /> 
					            </p> 
					            <p class="checkbox">
					                <label for="prior_dsc"><?php _e( 'Prior DCS', 'travelo' ) ?></label>
					                <input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="No" /><span>No</span>
					            </p>            
					            <div class="clear"></div>

					            <h3 class="info_header">INSURANCE</h3>
					            <p class="checkbox">
					                <label for="have_dive_insurance"><?php _e( 'Do you have dive insurance?', 'travelo' ) ?></label><br>
					                <input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="No" /><span>No</span>
					            </p> 

					            <p> 
					                <input type="text" name="dive_insurance_no" id="dive_insurance_no" class="input-text input-large full-width" placeholder="<?php _e( 'If yes please provide number', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $dive_insurance_no ) ); ?>" size="25" /> 
					            </p>
					             
					            <p class="checkbox">
					                <label for="need_dive_insurance"><?php _e( 'Do you need dive insurance?', 'travelo' ) ?></label><br>
					                <input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="No" /><span>No</span>
					            </p>            
					            <div class="clear"></div>
					            <br>
					            <h3 class="info_header">EQUIPMENT INFORMATION</h3>
					            <p class="checkbox fullwidth">
					                <label for="rent_diving_equipment"><?php _e( 'Do you want to rent diving equipment? ', 'travelo' ) ?></label><br>
					                <input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="No" /><span>No</span>
					            </p> 
					            <h4>If “yes” please provide the following sizes:</h4>
					            <p class="checkbox fullwidth">
					                <label for="size_bcd"><?php _e( 'BCD: ', 'travelo' ) ?><br>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXS" /><span>XXS</span>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XS" /><span>XS</span>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="S" /><span>S</span>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="M" /><span>M</span>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="L" /><span>L</span>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XL" /><span>XL</span>
					                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXL" /><span>XXL</span>
					            </p>
					             
					            <p class="checkbox fullwidth">
					                <label for="size_websuit"><?php _e( 'Wetsuit: ', 'travelo' ) ?></label><br>
					                <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XS" /><span>XS</span>
					                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="S" /><span>S</span>
					                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="M" /><span>M</span>
					                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="L" /><span>L</span>
					                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XL" /><span>XL</span>
					                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XXL" /><span>XXL</span>
					            </p> 
					            <p class="checkbox fullwidth">
					                <label for="size_fins"><?php _e( 'Fins: ', 'travelo' ) ?></label><br>
					                <input type="radio" name="size_fins" id="size_fins" class="input" value="36-37" /><span>36-37</span>
					                 <input type="radio" name="size_fins" id="size_fins" class="input" value="38-39" /><span>38-39</span>
					                 <input type="radio" name="size_fins" id="size_fins" class="input" value="40-41" /><span>40-41</span>
					                 <input type="radio" name="size_fins" id="size_fins" class="input" value="42-43" /><span>42-43</span>
					                 <input type="radio" name="size_fins" id="size_fins" class="input" value="44-45" /><span>44-45</span>
					            </p>
					            <p class="checkbox">
					                <label for="regulator"><?php _e( 'Regulator ', 'travelo' ) ?></label><br>
					                <input type="radio" name="regulator" id="regulator" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="regulator" id="regulator" class="input" value="No" /><span>No</span>
					            </p>
					            <p class="checkbox">
					                <label for="mask"><?php _e( 'Mask', 'travelo' ) ?></label><br>
					                <input type="radio" name="mask" id="mask" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="mask" id="mask" class="input" value="No" /><span>No</span>
					            </p>
					            <h4>*If you are unsure about sizing please state your:</h4> 
					            <p> 
					                <input type="text" name="weight" id="Weight" class="input-text input-large full-width" placeholder="<?php _e( 'Weight', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $weight ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="height" id="height" class="input-text input-large full-width" placeholder="<?php _e( 'Height', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $height ) ); ?>" size="25" /> 
					            </p>
					            <p> 
					                <input type="text" name="shoe_size" id="shoe_size" class="input-text input-large full-width" placeholder="<?php _e( 'Shoe Size (EU)', 'trav' ); ?>" value="<?php echo esc_attr( wp_unslash( $shoe_size ) ); ?>" size="25" />  
					            </p>              
					            <p class="checkbox">
					                <label for="need_din"><?php _e( 'Do you need a DIN? ', 'travelo' ) ?></label><br>
					                <input type="radio" name="need_din" id="need_din" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="need_din" id="need_din" class="input" value="No" /><span>No</span>
					            </p>
					            <p class="checkbox fullwidth">
					                <label for="rent_dive_computer"><?php _e( 'Do you want to rent a Dive Computer? ', 'travelo' ) ?></label><br>
					                <input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="Yes" /><span>Yes</span>
					                <input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="No" /><span>No</span>
					            </p>

					            <p class="checkbox fullwidth"> 
					            
					                <label for="optional_course"><?php _e( 'Courses (optional) ', 'travelo' ) ?></label>  <br>               
					                <input type="radio" name="optional_course" id="optional_course" class="input" value="DSD" /><span>DSD (Discover Scuba Diving)</span>
					                <input type="radio" name="optional_course" id="optional_course" class="input" value="Refresher" /><span>Refresher</span>
					                <input type="radio" name="optional_course" id="optional_course" class="input" value="Open Water Course" /><span>Open Water Course</span> <br>
					                <input type="radio" name="optional_course" id="optional_course" class="input" value="Advanced Open Water Course" /><span>Advanced Open Water Course</span>
					                <input type="radio" name="optional_course" id="optional_course" class="input" value="Adventure Deep Dive" /><span>Adventure Deep Dive</span>
					                <input type="radio" name="optional_course" id="optional_course" class="input" value="Specialty" /><span>Specialty</span>					        
					            </p>      
					            <div class="clear"></div> 

					            <input type="hidden" name="redirect_to" value="<?php echo esc_url( add_query_arg( 'checkemail', 'registered', trav_get_current_page_url() ) ); ?>">
								<button type="submit" class="btn-large full-width sky-blue1"><?php _e('REGISTER', 'trav')?></button>
								<p><br />
									<?php _e( 'Already a member?', 'trav' ); ?>
									<a href="<?php echo esc_url( $login_url );?>" class="underline"><?php _e( 'Login', 'trav' ); ?></a>
								</p>								
							</form>
						<?php } else if ( isset( $_GET['action'] ) && ( $_GET['action'] == 'lostpassword' ) ) { ?>
							<form name="lostpasswordform"  class="login-form" action="<?php echo esc_url( wp_lostpassword_url() ) ?>" method="post">
								<div class="form-group">
									<input type="text" name="user_login"  class="input-text input-large full-width" placeholder="<?php _e( 'Username or E-mail:', 'trav' ); ?>" value="" size="20"></label>
								</div>
								<button type="submit" class="btn-large full-width sky-blue1"><?php _e('Get New Password', 'trav')?></button>
								<input type="hidden" name="redirect_to" value="<?php echo esc_url( add_query_arg( 'checkemail', 'confirm', trav_get_current_page_url() ) ); ?>">
								<p><br />
									<a href="<?php echo esc_url( $login_url );?>" class="underline"><?php _e( 'Login', 'trav' ); ?></a> | 
									<a href="<?php echo esc_url( $signup_url ); ?>" class="underline"><?php _e( "Sign Up", 'trav' ) ?></a>
								</p>
							</form>
						<?php } else { ?>
							<form name="loginform" class="login-form" action="<?php echo esc_url( wp_login_url() )?>" method="post">
								<?php if ( ! empty( $_GET['login'] ) && ( $_GET['login'] == 'failed' ) ) { ?>
									<div class="alert alert-info"><span class="message"><?php _e( 'Invalid username or password','trav' ); ?></span><span class="close"></span></div>
								<?php } ?>
								<div class="form-group">
									<input type="text" name="log" class="input-text input-large full-width" placeholder="<?php _e( 'Enter your user name', 'trav' ); ?>" value="<?php echo empty($_GET['user']) ? '' : esc_attr( $_GET['user'] ) ?>">
								</div>
								<div class="form-group">
									<input type="password" name="pwd" class="input-text input-large full-width" placeholder="<?php _e( 'Enter your password', 'trav' ); ?>">
								</div>
								<div class="form-group" style="text-align:left;">
									<a href="<?php echo esc_url( add_query_arg( 'action', 'lostpassword', $login_url ) ); ?>" class="underline pull-right"><?php _e( 'Forgot password?', 'trav' ); ?></a>
									<div class="checkbox checkbox-inline" style="margin-left:0;">
										<label>
											<input type="checkbox" name="rememberme" tabindex="3" value="forever"> <?php _e( 'Remember my details', 'trav' ); ?>
										</label>
									</div>
								</div>
								<button type="submit" class="btn-large full-width sky-blue1"><?php _e('LOGIN TO YOUR ACCOUNT', 'trav')?></button>
								<input type="hidden" name="redirect_to" value="<?php echo esc_url( $redirect_url_on_login ) ?>">
								<p><br />
									<?php _e( "Don't have an account?", 'trav' ) ?>
									<a href="<?php echo esc_url( $signup_url ); ?>" class="underline"><?php _e( "Sign Up", 'trav' ) ?></a>
								</p>
							</form>
						<?php }?>
					</div>
				</div>
			</div>
		</section>
		<footer id="footer">
			<div class="footer-wrapper">
				<div class="container">
					<?php if ( has_nav_menu( 'header-menu' ) ) {
							wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => 'nav', 'container_id' => 'main-menu', 'container_class'=>'inline-block hidden-mobile', 'menu_class' => 'menu', 'walker'=>new Trav_Walker_Nav_Menu ) ); 
						} else { ?>
							<nav id="main-menu" class="inline-block hidden-mobile">
								<ul class="menu">
									<li class="menu-item"><a href="<?php echo esc_url( home_url() ); ?>"><?php _e('Home', "trav"); ?></a></li>
									<li class="menu-item"><a href="<?php echo esc_url( admin_url('nav-menus.php') ); ?>"><?php _e('Configure', "trav"); ?></a></li>
								</ul>
							</nav>
					<?php } ?>
					<div class="copyright">
						<p>&copy; <?php echo esc_html( $trav_options['copyright'] ); ?></p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<?php
	endwhile;
}
?>
<?php wp_footer(); ?>
</body>
</html>