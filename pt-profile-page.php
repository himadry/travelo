<?php
/***
## Template Name: User profile page
**/
get_header();
 ?>
<style type="text/css">
	.profile_page p{font-size: 18px;margin-bottom: 5px;	}
	.edit_profile a.btn-primary{margin: 10px 0 20px 0; background: #98ce44;border: none;}
</style>

<section id="content">
	<div class="container">
		<div id="main" class="entry-content">
		    <?php if(is_user_logged_in()){
				global $current_user;
      			get_currentuserinfo();
      			$current_user_id = $current_user->ID;	
		    ?>
			<div class="profile_page">
				<h2>PROFILE</h2>
				<div class="col-md-5">
					<p>User Name: <?php echo $current_user->user_login; ?></p>
				</div>
				<div class="col-md-5">
					<p>Email: <?php echo $current_user->user_email ; ?></p>
				</div>				
				<div class="clearfix"></div>

				<h2>PERSONAL INFORMATION</h2>
				
				<div class="col-md-5">
					<p>First Name: <?php if(get_user_meta( $current_user_id, 'first_name', true )){ echo get_user_meta( $current_user_id, 'first_name', true );} else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Middle Initial: <?php if(get_user_meta( $current_user_id, 'middle_initial', true )){echo get_user_meta( $current_user_id, 'middle_initial', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Last Name: <?php if(get_user_meta( $current_user_id, 'last_name', true )){echo get_user_meta( $current_user_id, 'last_name', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Gender: <?php if(get_user_meta( $current_user_id, 'gender', true )){echo get_user_meta( $current_user_id, 'gender', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Nationality: <?php if(get_user_meta( $current_user_id, 'nationality', true )){ echo get_user_meta( $current_user_id, 'nationality', true );
					}else{ echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Date of Birth: <?php if(get_user_meta( $current_user_id, 'birthday', true )){echo get_user_meta( $current_user_id, 'birthday', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Passport Number: <?php if(get_user_meta( $current_user_id, 'passport_number', true )){echo get_user_meta( $current_user_id, 'passport_number', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Dietary Requirements: <?php if(get_user_meta( $current_user_id, 'dietary_req', true )){echo get_user_meta( $current_user_id, 'dietary_req', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Allergies: <?php  if(get_user_meta( $current_user_id, 'allergies', true )){echo get_user_meta( $current_user_id, 'allergies', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="clearfix"></div>

				<h2>CONTACT DETAILS</h2>
				
				<div class="col-md-5">
					<p>Home Address: <?php  if(get_user_meta( $current_user_id, 'home_address', true )){echo get_user_meta( $current_user_id, 'home_address', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Mobile Phone: <?php if(get_user_meta( $current_user_id, 'phone', true )){ echo get_user_meta( $current_user_id, 'phone', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Hotel Name: <?php if(get_user_meta( $current_user_id, 'hotel_name', true )){ echo get_user_meta( $current_user_id, 'hotel_name', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Date Check in: <?php if(get_user_meta( $current_user_id, 'date_check', true )){echo get_user_meta( $current_user_id, 'date_check', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="clearfix"></div>

				<h2>EMERGENCY CONTACT DETAILS</h2>
				
				<div class="col-md-5">
					<p>Name: <?php if(get_user_meta( $current_user_id, 'emergency_name', true )){echo get_user_meta( $current_user_id, 'emergency_name', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Relationship: <?php if(get_user_meta( $current_user_id, 'emergency_relationship', true )){echo get_user_meta( $current_user_id, 'emergency_relationship', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Mobile Phone: <?php if(get_user_meta( $current_user_id, 'emergency_mobile_no', true )){ echo get_user_meta( $current_user_id, 'emergency_mobile_no', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Home Phone: <?php if(get_user_meta( $current_user_id, 'emergency_home_phone', true )){ echo get_user_meta( $current_user_id, 'emergency_home_phone', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="clearfix"></div>
				
				<h2>DIVING DETAILS</h2>
				
				<div class="col-md-5">
					<p>Certification Agency: <?php if(get_user_meta( $current_user_id, 'diving_certificate_agency', true )){ echo get_user_meta( $current_user_id, 'diving_certificate_agency', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Certification Level: <?php if(get_user_meta( $current_user_id, 'diving_certification_level', true )){echo get_user_meta( $current_user_id, 'diving_certification_level', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Number of Dives: <?php if(get_user_meta( $current_user_id, 'number_of_dives', true )){echo get_user_meta( $current_user_id, 'number_of_dives', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Date of Last Dive: <?php if(get_user_meta( $current_user_id, 'date_of_last_dive', true )){echo get_user_meta( $current_user_id, 'date_of_last_dive', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Prior DCS: <?php if(get_user_meta( $current_user_id, 'prior_dsc', true )){ echo get_user_meta( $current_user_id, 'prior_dsc', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="clearfix"></div>
				
				<h2>INSURANCE</h2>
				
				<div class="col-md-5">
					<p>Do you have dive insurance?: <?php if(get_user_meta( $current_user_id, 'have_dive_insurance', true )){echo get_user_meta( $current_user_id, 'have_dive_insurance', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Dive insurance number: <?php if(get_user_meta( $current_user_id, 'dive_insurance_no', true )){ echo get_user_meta( $current_user_id, 'dive_insurance_no', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Do you need dive insurance?: <?php if(get_user_meta( $current_user_id, 'need_dive_insurance', true )){echo get_user_meta( $current_user_id, 'need_dive_insurance', true );}else{echo 'N/A';} ?></p>
				</div>	
				
				<div class="clearfix"></div>
				
				<h2>EQUIPMENT INFORMATION</h2>
				
				<div class="col-md-5">
					<p>Do you want to rent diving equipment? : <?php if(get_user_meta( $current_user_id, 'rent_diving_equipment', true )){echo get_user_meta( $current_user_id, 'rent_diving_equipment', true );}else{echo 'N/A';}  ?></p>
				</div>
				
				<div class="col-md-5">
					<p>BCD: <?php if(get_user_meta( $current_user_id, 'size_bcd', true )){echo get_user_meta( $current_user_id, 'size_bcd', true );}else{echo 'N/A';}  ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Wetsuit: <?php if(get_user_meta( $current_user_id, 'size_websuit', true )){echo get_user_meta( $current_user_id, 'size_websuit', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Fins: <?php if(get_user_meta( $current_user_id, 'size_fins', true )){ echo get_user_meta( $current_user_id, 'size_fins', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Regulator: <?php if(get_user_meta( $current_user_id, 'regulator', true )){echo get_user_meta( $current_user_id, 'regulator', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Mask: <?php if(get_user_meta( $current_user_id, 'mask', true )){echo get_user_meta( $current_user_id, 'mask', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<!-- <p>*If you are unsure about sizing please state your:</p> -->
					<p>Weight: <?php if(get_user_meta( $current_user_id, 'weight', true )){echo get_user_meta( $current_user_id, 'weight', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Height: <?php if(get_user_meta( $current_user_id, 'height', true )){echo get_user_meta( $current_user_id, 'height', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Shoe Size (EU): <?php if(get_user_meta( $current_user_id, 'shoe_size', true )){echo get_user_meta( $current_user_id, 'shoe_size', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Do you need a DIN?: <?php if(get_user_meta( $current_user_id, 'need_din', true )){echo get_user_meta( $current_user_id, 'need_din', true );}else{echo 'N/A';} ?></p>
				</div>	
				
				<div class="col-md-5">
					<p>Do you want to rent a Dive Computer? : <?php if(get_user_meta( $current_user_id, 'rent_dive_computer', true )){echo get_user_meta( $current_user_id, 'rent_dive_computer', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5">
					<p>Courses (optional) : <?php if(get_user_meta( $current_user_id, 'optional_course', true )){echo get_user_meta( $current_user_id, 'optional_course', true );}else{echo 'N/A';} ?></p>
				</div>
				
				<div class="col-md-5 edit_profile">
					<a href="<?php echo site_url().'/edit-profile' ?>" class="btn btn-primary pull-right ">Edit Profile</a>
				</div>
			</div>
			<?php } else{ wp_redirect(site_url().'/wp-login.php'); } ?>
		</div>
	</div>
</section>

<?php get_footer();