<?php
get_header();

if ( have_posts() ) {
	while ( have_posts() ) : the_post();

		//init variables
		$tour_id = get_the_ID();
		$city = trav_tour_get_city( $tour_id );
		$country = trav_tour_get_country( $tour_id );
		$tour_meta = get_post_meta( $tour_id );

		$gallery_imgs = array_key_exists( 'trav_gallery_imgs', $tour_meta ) ? $tour_meta['trav_gallery_imgs'] : array();

		$calendar_desc = empty( $tour_meta['trav_tour_calendar_txt'] ) ? '' : $acc_meta['trav_tour_calendar_txt'][0];
		$show_gallery = 0;
		$show_calendar = 0;
		if ( array_key_exists( 'trav_tour_main_top', $tour_meta ) ) {
			$main_top_meta = $tour_meta['trav_tour_main_top'];
			$show_gallery = in_array( 'gallery', $main_top_meta ) ? 1 : 0;			
			$show_calendar = in_array( 'calendar', $main_top_meta ) ? 1 : 0;
		}

		$tour_types = get_posts( $args );

		$date_from = ( isset( $_GET['date_from'] ) ) ? trav_tophptime( $_GET['date_from'] ) : date( trav_get_date_format('php') );
		$date_to = ( isset( $_GET['date_to'] ) ) ? trav_tophptime( $_GET['date_to'] ) : date( trav_get_date_format('php'), trav_strtotime( $date_from ) + 86400 * 30 );
		$repeated = get_post_meta( $tour_id, 'trav_tour_repeated', true );
		$multi_book = get_post_meta( $tour_id, 'trav_tour_multi_book', true );
		$isv_setting = get_post_meta( $tour_id, 'trav_post_media_type', true );
		$discount = get_post_meta( $tour_id, 'trav_tour_hot', true );
		$discount_rate = get_post_meta( $tour_id, 'trav_tour_discount_rate', true );
		$sc_list_pos = get_post_meta( $tour_id, 'trav_tour_sl_first', true );

		$schedule_types = trav_tour_get_schedule_types( $tour_id );

		// add to user recent activity
		trav_update_user_recent_activity( $tour_id ); ?>

		<section id="content">
			<div class="container tour-detail-page">
				<div class="row">
					<div id="main" class="col-sm-8 col-md-9">

					 <div class="tab-container style1" id="hotel-main-content">
							<ul class="tabs">

								<?php if ( ! empty( $gallery_imgs ) && $show_gallery ) { ?>
									<li class="active"><a data-toggle="tab" href="#photos-tab"><?php echo __( 'photos', 'trav' ) ?></a></li>
								<?php } ?>
								
								<?php if ( $show_calendar ) { ?>
									<!-- <li><a data-toggle="tab" href="#calendar-tab"><?php echo __( 'calendar', 'trav' ) ?></a></li> -->
								<?php } ?>

							</ul>

							<div class="tab-content">

								<?php if ( ! empty( $gallery_imgs ) && $show_gallery ) { ?>
									<div id="photos-tab" class="tab-pane fade active in">
										<div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
											<ul class="slides">
												<?php foreach ( $gallery_imgs as $gallery_img ) {
													echo '<li>' . wp_get_attachment_image( $gallery_img, 'full' ) . '</li>';
												} ?>
											</ul>
										</div>
										<div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
											<ul class="slides">
												<?php foreach ( $gallery_imgs as $gallery_img ) {
													echo '<li>' . wp_get_attachment_image( $gallery_img, 'widget-thumb' ) . '</li>';
												} ?>
											</ul>
										</div>
									</div>
								<?php } ?>

								<?php  if ( $show_calendar ) { ?>
									<div id="calendar-tab" class="tab-pane fade">
										<div class="row">

											<div class="col-sm-6 col-md-4 no-lpadding">
												<label><?php _e( 'SELECT MONTH', 'trav' );?></label>
												<div class="selector">
													<select class="full-width" id="select-month">
														<?php for ( $i = 0; $i<12; $i++ ) {
															$year_month = mktime( 0, 0, 0, date("m") + $i, 1, date("Y") );
															echo '<option value="' . date( 'Y-n', $year_month ) . '"> ' . __( date('F', $year_month ), 'trav' ) . date(' Y', $year_month ) . '</option>';
														} ?>
													</select>
												</div>
											</div>
				
											
											<?php if ( ! empty( $tour_types) ) { ?>
													<div class="col-sm-6 col-md-4 no-lpadding">
														<label><?php _e( 'SELECT TOUR TYPE', 'trav' );?></label>
														<div class="selector">
															<select class="full-width" id="select-tour-type">
																<option value=""><?php _e( 'All Tour Types', 'trav' ); ?></option>
																<?php
																	foreach ( $tour_types as $tour_type ) {
																		echo '<option value="' . esc_attr( $tour_type->name ) . '">' . get_the_title($tour_type->name) . '</option>';
																	}
																?>
															</select>
														</div>
													</div>
											<?php } ?>

										</div>
										<div class="row">
											<?php if ( ! empty( $calendar_desc ) ) { ?>
												<div class="col-sm-8">

												
													<div class="calendar"></div>
													<div class="calendar-legend">
														<label class="available"><?php echo __( 'available', 'trav' ) ?></label>
														<label class="unavailable"><?php echo __( 'unavailable', 'trav' ) ?></label>
														<label class="past"><?php echo __( 'past', 'trav' ) ?></label>
													</div>
												</div>
												<div class="col-sm-4">
													<p class="description">
														<?php
															echo esc_html( $calendar_desc )
														?>
													</p>
												</div>
											<?php } else { ?>
												<div class="calendar"></div>
												<div class="calendar-legend">
													<label class="available"><?php echo __( 'available', 'trav' ) ?></label>
													<label class="unavailable"><?php echo __( 'unavailable', 'trav' ) ?></label>
													<label class="past"><?php echo __( 'past', 'trav' ) ?></label>
												</div>
											<?php } ?>

										</div>
									</div>
								<?php } ?>
							</div>
						</div>





						<div <?php post_class(); ?>>

							<div id="tour-details" class="travelo-box">
								<?php //if ( ! empty( $repeated ) ): ?>
									<!-- <form id="check_availability_form" method="post">
										<input type="hidden" name="tour_id" value="<?php echo esc_attr( $tour_id ); ?>">
										<input type="hidden" name="action" value="tour_get_available_schedules">
										<?php wp_nonce_field( 'post-' . $tour_id, '_wpnonce', false ); ?>
										<div class="update-search clearfix">
											<div class="alert alert-error" style="display:none;"><span class="message"><?php _e( 'Please select check in date.','trav' ); ?></span><span class="close"></span></div>
											<h4><?php _e( 'Check Availability', 'trav' ) ?></h4>
											<div class="col-md-6">
												<div class="row">
													<div class="col-xs-6">
														<label><?php _e( 'From','trav' ); ?></label>
														<div class="datepicker-wrap validation-field from-today">
															<input name="date_from" type="text" placeholder="<?php echo trav_get_date_format('html'); ?>" class="input-text full-width" value="<?php echo $date_from; ?>" />
														</div>
													</div>
													<div class="col-xs-6">
														<label><?php _e( 'To','trav' ); ?></label>
														<div class="datepicker-wrap validation-field from-today">
															<input name="date_to" type="text" placeholder="<?php echo trav_get_date_format('html'); ?>" class="input-text full-width" value="<?php echo $date_to;?>" />
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-3">
												<label class="visible-md visible-lg">&nbsp;</label>
												<div class="row">
													<div class="col-xs-12">
														<button id="check_availability" data-animation-duration="1" data-animation-type="bounce" class="full-width icon-check animated bounce" type="submit"><?php _e( "UPDATE", "trav" ); ?></button>
													</div>
												</div>
											</div>
										</div>
									</form> -->
								<?php //endif; ?>

								<?php if ( empty( $sc_list_pos ) ) : ?>

									<div class="entry-content"><?php the_content(); ?></div>
									<div id="schedule-list">
										<?php trav_tour_get_schedule_list_html( array( 'tour_id'=>$tour_id, 'date_from'=>$date_from, 'date_to'=>$date_to ) ); ?>
									</div>

								<?php else : ?>

									<div id="schedule-list">
										<?php trav_tour_get_schedule_list_html( array( 'tour_id'=>$tour_id, 'date_from'=>$date_from, 'date_to'=>$date_to ) ); ?>
									</div>
									<div class="entry-content"><?php the_content(); ?></div>

								<?php endif; ?>

							</div>
						</div>
					</div>
					<div class="sidebar col-sm-4 col-md-3">
						<?php generated_dynamic_sidebar(); ?>
					</div>
				</div>
			</div>
		</section>
<?php endwhile;
}
get_footer();