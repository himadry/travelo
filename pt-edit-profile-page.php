<?php
/***
## Template Name: Edit profile page
**/
get_header();
 ?>
<style type="text/css">
	.profile_page p{font-size: 18px;margin-bottom: 5px;	}
	.profile_page p span{width: 190px;float: left; }
	input[type="radio"]{margin: 4px;}
	.edit_profile input[type="submit"]{margin: 10px 0 20px 0; }
	.edit_profile input[type="submit"]:focus{background: #98ce44;border: 1px solid #98ce44;}
	.profile_page input[type="password"]{background: none;border: 1px solid;}

</style>
<!-- update usermeta -->
<?php 


 ?>

<section id="content">
	<div class="container">
		<div id="main" class="entry-content">
		<?php if(is_user_logged_in()){
				global $current_user;
      			get_currentuserinfo();
      			$current_user_id = $current_user->ID;

      			if(isset($_POST['profile_update'])){      				

	      			if(isset($_POST['password'])){
						$password = $_POST['password'];
					}	
					if(isset($_POST['first_name'])){
						$first_name = trim($_POST['first_name']);
					}
					if(isset($_POST['middle_initial'])){
						$middle_initial = trim($_POST['middle_initial']);
					}
					if(isset($_POST['last_name'])){
						$last_name = trim($_POST['last_name']);
					}
					if(isset($_POST['gender'])){
						$gender = trim($_POST['gender']);
					}
					if(isset($_POST['nationality'])){
						$nationality = trim($_POST['nationality']);
					}
					if(isset($_POST['date_of_birth'])){
						$date_of_birth = trim($_POST['date_of_birth']);
					}
					if(isset($_POST['passport_number'])){
						$passport_number = trim($_POST['passport_number']);
					}
					if(isset($_POST['dietary_req'])){
						$dietary_req = trim($_POST['dietary_req']);
					}
					if(isset($_POST['allergies'])){
						$allergies = trim($_POST['allergies']);
					}

					if(isset($_POST['home_address'])){
						$home_address = trim($_POST['home_address']);
					}
					if(isset($_POST['phone'])){
						$phone = trim($_POST['phone']);
					}
					if(isset($_POST['hotel_name'])){
						$hotel_name = trim($_POST['hotel_name']);
					}
					if(isset($_POST['date_check'])){
						$date_check = trim($_POST['date_check']);
					}

					if(isset($_POST['emergency_name'])){
						$emergency_name = trim($_POST['emergency_name']);
					}
					if(isset($_POST['emergency_relationship'])){
						$emergency_relationship = trim($_POST['emergency_relationship']);
					}
					if(isset($_POST['emergency_mobile_no'])){
						$emergency_mobile_no = trim($_POST['emergency_mobile_no']);
					}
					if(isset($_POST['emergency_home_phone'])){
						$emergency_home_phone = trim($_POST['emergency_home_phone']);
					}

					if(isset($_POST['diving_certificate_agency'])){
						$diving_certificate_agency = trim($_POST['diving_certificate_agency']);
					}
					if(isset($_POST['diving_certification_level'])){
						$diving_certification_level = trim($_POST['diving_certification_level']);
					}
					if(isset($_POST['number_of_dives'])){
						$number_of_dives = trim($_POST['number_of_dives']);
					}
					if(isset($_POST['date_of_last_dive'])){
						$date_of_last_dive = trim($_POST['date_of_last_dive']);
					}
					if(isset($_POST['prior_dsc'])){
						$prior_dsc = trim($_POST['prior_dsc']);
					}
					
					if(isset($_POST['have_dive_insurance'])){
						$have_dive_insurance = trim($_POST['have_dive_insurance']);
					}
					if(isset($_POST['dive_insurance_no'])){
						$dive_insurance_no = trim($_POST['dive_insurance_no']);
					}
					if(isset($_POST['need_dive_insurance'])){
						$need_dive_insurance = trim($_POST['need_dive_insurance']);
					}
					
					if(isset($_POST['rent_diving_equipment'])){
						$rent_diving_equipment = trim($_POST['rent_diving_equipment']);
					}
					if(isset($_POST['size_bcd'])){
						$size_bcd = trim($_POST['size_bcd']);
					}
					if(isset($_POST['size_websuit'])){
						$size_websuit = trim($_POST['size_websuit']);
					}
					if(isset($_POST['size_fins'])){
						$size_fins = trim($_POST['size_fins']);
					}
					if(isset($_POST['regulator'])){
						$regulator = trim($_POST['regulator']);
					}		
					if(isset($_POST['mask'])){
						$mask = trim($_POST['mask']);
					}
					if(isset($_POST['weight'])){
						$weight = trim($_POST['weight']);
					}
					if(isset($_POST['height'])){
						$height = trim($_POST['height']);
					}
					if(isset($_POST['shoe_size'])){
						$shoe_size = trim($_POST['shoe_size']);
					}
					if(isset($_POST['need_din'])){
						$need_din = trim($_POST['need_din']);
					}
					if(isset($_POST['rent_dive_computer'])){
						$rent_dive_computer = trim($_POST['rent_dive_computer']);
					}
					if(isset($_POST['optional_course'])){
						$optional_course = trim($_POST['optional_course']);
					}
					if($password){
						wp_update_user( array( 'ID' => $current_user_id, 'user_pass' => $password ) );
					}
					update_user_meta( $current_user_id, 'first_name', $first_name );
					update_user_meta( $current_user_id, 'middle_initial', $middle_initial );
					update_user_meta( $current_user_id, 'last_name', $last_name );
					update_user_meta( $current_user_id, 'gender', $gender );
					update_user_meta( $current_user_id, 'nationality', $nationality );
					update_user_meta( $current_user_id, 'date_of_birth', $date_of_birth );
					update_user_meta( $current_user_id, 'passport_number', $passport_number );
					update_user_meta( $current_user_id, 'dietary_req', $dietary_req );
					update_user_meta( $current_user_id, 'allergies', $allergies );
					update_user_meta( $current_user_id, 'home_address', $home_address );
					update_user_meta( $current_user_id, 'phone', $phone );
					update_user_meta( $current_user_id, 'hotel_name', $hotel_name );
					update_user_meta( $current_user_id, 'date_check', $date_check );
					update_user_meta( $current_user_id, 'emergency_name', $emergency_name );
					update_user_meta( $current_user_id, 'emergency_relationship', $emergency_relationship );
					update_user_meta( $current_user_id, 'emergency_mobile_no', $emergency_mobile_no );
					update_user_meta( $current_user_id, 'emergency_home_phone', $emergency_home_phone );
					update_user_meta( $current_user_id, 'diving_certificate_agency', $diving_certificate_agency );
					update_user_meta( $current_user_id, 'diving_certification_level', $diving_certification_level );
					update_user_meta( $current_user_id, 'number_of_dives', $number_of_dives );
					update_user_meta( $current_user_id, 'date_of_last_dive', $date_of_last_dive );
					update_user_meta( $current_user_id, 'prior_dsc', $prior_dsc );
					update_user_meta( $current_user_id, 'have_dive_insurance', $have_dive_insurance );
					update_user_meta( $current_user_id, 'dive_insurance_no', $dive_insurance_no );
					update_user_meta( $current_user_id, 'need_dive_insurance', $need_dive_insurance );
					update_user_meta( $current_user_id, 'rent_diving_equipment', $rent_diving_equipment );
					update_user_meta( $current_user_id, 'size_bcd', $size_bcd );
					update_user_meta( $current_user_id, 'size_websuit', $size_websuit );
					update_user_meta( $current_user_id, 'size_fins', $size_fins );
					update_user_meta( $current_user_id, 'regulator', $regulator );
					update_user_meta( $current_user_id, 'mask', $mask );
					update_user_meta( $current_user_id, 'weight', $weight );
					update_user_meta( $current_user_id, 'height', $height );
					update_user_meta( $current_user_id, 'shoe_size', $shoe_size );
					update_user_meta( $current_user_id, 'need_din', $need_din );
					update_user_meta( $current_user_id, 'rent_dive_computer', $rent_dive_computer );
					update_user_meta( $current_user_id, 'optional_course', $optional_course );
					
					echo '<h2 class="text-success">Successfully Update</h2>';

				}
      				
		    ?>
			<div class="profile_page">
				<h2>PROFILE</h2>
				<form method="post" action="" >
					<!-- <div class="col-md-5">
						<p><span>User Name:</span> <input type="text" name="" value=" <?php echo $current_user->user_login; ?>"></p>
					</div>
					<div class="col-md-5">
						<p><span>Email:</span> <input type="text" name="" value=" <?php echo $current_user->user_email ; ?>"></p>
					</div> -->
					 <div class="col-md-5">
						<p><span>Update Password:</span> <input type="password" name="password" value=""></p>
					</div>
					<!--<div class="col-md-5">
						<p><span>Repeated Password:</span> <input type="text" name="repeat_pass" value=""></p>
					</div> -->				
					<div class="clearfix"></div>

					<h2>PERSONAL INFORMATION</h2>
					
					<div class="col-md-5">
						<p><span>First Name:</span> <input type="text" name="first_name" value=" <?php echo trim(get_user_meta( $current_user_id, 'first_name', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Middle Initial:</span><input type="text" name="middle_initial" value=" <?php echo trim(get_user_meta( $current_user_id, 'middle_initial', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Last Name:</span> <input type="text" name="last_name" value=" <?php echo trim(get_user_meta( $current_user_id, 'last_name', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Gender:</span> <input type="text" name="gender" value=" <?php echo trim(get_user_meta( $current_user_id, 'gender', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Nationality:</span> <input type="text" name="nationality" value=" <?php echo trim(get_user_meta( $current_user_id, 'nationality', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Date of Birth:</span> <input type="text" name="date_of_birth" value=" <?php echo trim(get_user_meta( $current_user_id, 'date_of_birth', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Passport Number:</span> <input type="text" name="passport_number" value=" <?php echo trim(get_user_meta( $current_user_id, 'passport_number', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Dietary Requirements:</span> <input type="text" name="dietary_req" value=" <?php echo trim(get_user_meta( $current_user_id, 'dietary_req', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Allergies:</span> <input type="text" name="allergies" value=" <?php echo trim(get_user_meta( $current_user_id, 'allergies', true )); ?>"></p>
					</div>
					
					<div class="clearfix"></div>

					<h2>CONTACT DETAILS</h2>
				
					<div class="col-md-5">
						<p><span>Home Address:</span> <input type="text" name="home_address" value=" <?php echo trim(get_user_meta( $current_user_id, 'home_address', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Mobile Phone:</span> <input type="text" name="phone" value=" <?php echo trim(get_user_meta( $current_user_id, 'phone', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Hotel Name:</span> <input type="text" name="hotel_name" value=" <?php echo trim(get_user_meta( $current_user_id, 'hotel_name', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Date Check in:</span> <input type="text" name="date_check" value=" <?php echo trim(get_user_meta( $current_user_id, 'date_check', true )); ?>"></p>
					</div>
					
					<div class="clearfix"></div>

					<h2>EMERGENCY CONTACT DETAILS</h2>
					
					<div class="col-md-5">
						<p><span>Name:</span> <input type="text" name="emergency_name" value=" <?php echo trim(get_user_meta( $current_user_id, 'emergency_name', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Relationship:</span> <input type="text" name="emergency_relationship" value=" <?php echo trim(get_user_meta( $current_user_id, 'emergency_relationship', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Mobile Phone:</span> <input type="text" name="emergency_mobile_no" value=" <?php echo trim(get_user_meta( $current_user_id, 'emergency_mobile_no', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Home Phone:</span> <input type="text" name="emergency_home_phone" value=" <?php echo trim(get_user_meta( $current_user_id, 'emergency_home_phone', true )); ?>"></p>
					</div>
				
					<div class="clearfix"></div>
					
					<h2>DIVING DETAILS</h2>
					
					<div class="col-md-5">
						<p><span>Certification Agency:</span> <input type="text" name="diving_certificate_agency" value=" <?php echo trim(get_user_meta( $current_user_id, 'diving_certificate_agency', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Certification Level:</span> <input type="text" name="diving_certification_level" value=" <?php echo trim(get_user_meta( $current_user_id, 'diving_certification_level', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Number of Dives:</span> <input type="text" name="number_of_dives" value=" <?php echo trim(get_user_meta( $current_user_id, 'number_of_dives', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Date of Last Dive:</span> <input type="text" name="date_of_last_dive" value=" <?php echo trim(get_user_meta( $current_user_id, 'date_of_last_dive', true )); ?>"></p>
					</div>
				
					<div class="col-md-5">
						<p><span>Prior DCS:</span> 
						<input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="Yes" <?php if(get_user_meta( $current_user_id, 'prior_dsc', true )== 'Yes') {echo 'checked';}  ?> />Yes
	                    <input type="radio" name="prior_dsc" id="prior_dsc" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'prior_dsc', true )== 'No')  {echo 'checked';} ?> />No
						</p>
					</div>
					
					<div class="clearfix"></div>
					
					<h2>INSURANCE</h2>
					
					<div class="col-md-5">
						<p><span>Do you have dive insurance?:</span> 
						<input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="Yes" <?php if(get_user_meta( $current_user_id, 'have_dive_insurance', true )== 'Yes') {echo 'checked';}  ?> />Yes
	                    <input type="radio" name="have_dive_insurance" id="have_dive_insurance" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'have_dive_insurance', true )== 'No')  {echo 'checked';} ?> />No
						</p>
					</div>
					
					<div class="col-md-5">
						<p><span>Dive insurance number: </span> <input type="text" name="dive_insurance_no" value=" <?php echo trim(get_user_meta( $current_user_id, 'dive_insurance_no', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Do you need dive insurance?: </span>
						<input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="Yes" <?php if(get_user_meta( $current_user_id, 'need_dive_insurance', true )== 'Yes') {echo 'checked';}  ?> />Yes
	                	<input type="radio" name="need_dive_insurance" id="need_dive_insurance" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'need_dive_insurance', true )== 'No')  {echo 'checked';} ?> />No
						</p>
					</div>	
				
					<div class="clearfix"></div>
					
					<h2>EQUIPMENT INFORMATION</h2>
					
					<div class="col-md-5">
						<p><span>Do you want to rent diving equipment? :</span> 
						<input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="Yes" <?php if(get_user_meta( $current_user_id, 'rent_diving_equipment', true )== 'Yes') {echo 'checked';}  ?> />Yes
	                	<input type="radio" name="rent_diving_equipment" id="rent_diving_equipment" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'rent_diving_equipment', true )== 'No')  {echo 'checked';} ?> />No
						</p>
					</div>				
					
					<div class="col-md-6">
						<p><span>BCD:</span> 
						<input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXS" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'XXS')  {echo 'checked';} ?> />XXS
		                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XS" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'XS')  {echo 'checked';} ?> />XS
		                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="S" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'S')  {echo 'checked';} ?> />S
		                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="M" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'M')  {echo 'checked';} ?> />M
		                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="L" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'L')  {echo 'checked';} ?> />L
		                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XL" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'XL')  {echo 'checked';} ?> />XL
		                 <input type="radio" name="size_bcd" id="size_bcd" class="input" value="XXL" <?php if (get_user_meta( $current_user_id, 'size_bcd', true )== 'XXL')  {echo 'checked';} ?> />XXL
						</p>
					</div>
					
					<div class="col-md-5">
						<p><span>Wetsuit:</span> 
						<input type="radio" name="size_websuit" id="size_websuit" class="input" value="XS" <?php if (get_user_meta( $current_user_id, 'size_websuit', true )== 'XS')  {echo 'checked';} ?> />XS
		                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="S" <?php if (get_user_meta( $current_user_id, 'size_websuit', true )== 'S')  {echo 'checked';} ?>/>S
		                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="M" <?php if (get_user_meta( $current_user_id, 'size_websuit', true )== 'M')  {echo 'checked';} ?> />M
		                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="L" <?php if (get_user_meta( $current_user_id, 'size_websuit', true )== 'L')  {echo 'checked';} ?> />L
		                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XL" <?php if (get_user_meta( $current_user_id, 'size_websuit', true )== 'XL')  {echo 'checked';} ?> />XL
		                 <input type="radio" name="size_websuit" id="size_websuit" class="input" value="XXL"  <?php if (get_user_meta( $current_user_id, 'size_websuit', true )== 'XXL')  {echo 'checked';} ?>/>XXL
						</p>
					</div>
					
					<div class="col-md-5">
						<p><span>Fins:</span>
						 <input type="radio" name="size_fins" id="size_fins" class="input" value="36-37" <?php if (get_user_meta( $current_user_id, 'size_fins', true )== '36-37')  {echo 'checked';} ?> />36-37
		                 <input type="radio" name="size_fins" id="size_fins" class="input" value="38-39" <?php if (get_user_meta( $current_user_id, 'size_fins', true )== '38-39')  {echo 'checked';} ?> />38-39
		                 <input type="radio" name="size_fins" id="size_fins" class="input" value="40-41" <?php if (get_user_meta( $current_user_id, 'size_fins', true )== '40-41')  {echo 'checked';} ?> />40-41
		                 <input type="radio" name="size_fins" id="size_fins" class="input" value="42-43" <?php if (get_user_meta( $current_user_id, 'size_fins', true )== '42-43')  {echo 'checked';} ?> />42-43
		                 <input type="radio" name="size_fins" id="size_fins" class="input" value="44-45" <?php if (get_user_meta( $current_user_id, 'size_fins', true )== '44-45')  {echo 'checked';} ?> />44-45
						 </p>
					</div>
					
					<div class="col-md-5">
						<p><span>Regulator:</span>
						 <input type="radio" name="regulator" id="regulator" class="input" value="Yes" <?php if (get_user_meta( $current_user_id, 'regulator', true )== 'Yes')  {echo 'checked';} ?> />Yes
	                	<input type="radio" name="regulator" id="regulator" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'regulator', true )== 'No')  {echo 'checked';} ?> />No
						 </p>
					</div>
					
					<div class="col-md-5">
						<p><span>Mask:</span>
						 <input type="radio" name="mask" id="mask" class="input" value="Yes" <?php if (get_user_meta( $current_user_id, 'mask', true )== 'Yes')  {echo 'checked';} ?> />Yes
	                	<input type="radio" name="mask" id="mask" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'mask', true )== 'No')  {echo 'checked';} ?> />No
						 </p>
					</div>
					
					<div class="col-md-5">
						<!-- <p>*If you are unsure about sizing please state your:</p> -->
						<p><span>Weight:</span> <input type="text" name="weight" value=" <?php echo trim(get_user_meta( $current_user_id, 'weight', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Height:</span> <input type="text" name="height" value=" <?php echo trim(get_user_meta( $current_user_id, 'height', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Shoe Size (EU):</span><input type="text" name="shoe_size" value=" <?php echo trim(get_user_meta( $current_user_id, 'shoe_size', true )); ?>"></p>
					</div>
					
					<div class="col-md-5">
						<p><span>Do you need a DIN?:</span>
						 <input type="radio" name="need_din" id="need_din" class="input" value="Yes" <?php if (get_user_meta( $current_user_id, 'need_din', true )== 'Yes')  {echo 'checked';} ?> />Yes
	                	<input type="radio" name="need_din" id="need_din" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'need_din', true )== 'No')  {echo 'checked';} ?> />No
						 </p>
					</div>	
					
					<div class="col-md-5">
						<p><span>Do you want to rent a Dive Computer? :</span>
						 <input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="Yes" <?php if (get_user_meta( $current_user_id, 'rent_dive_computer', true )== 'Yes')  {echo 'checked';} ?>/>Yes
	                	<input type="radio" name="rent_dive_computer" id="rent_dive_computer" class="input" value="No" <?php if (get_user_meta( $current_user_id, 'rent_dive_computer', true )== 'No')  {echo 'checked';} ?>/>No
						 </p>
					</div>
					
					<div class="col-md-5">
						<p><span>Courses (optional) :</span><br>
						 <input type="radio" name="optional_course" id="optional_course" class="input" value="DSD" <?php if (get_user_meta( $current_user_id, 'optional_course', true )== 'DSD')  {echo 'checked';} ?> />DSD (Discover Scuba Diving)	
						 				 
		                <input type="radio" name="optional_course" id="optional_course" class="input" value="Refresher" <?php if (get_user_meta( $current_user_id, 'optional_course', true )== 'Refresher')  {echo 'checked';} ?> />Refresher

		                <input type="radio" name="optional_course" id="optional_course" class="input" value="Open Water Course" <?php if (get_user_meta( $current_user_id, 'optional_course', true )== 'Open Water Course')  {echo 'checked';} ?> />Open Water Course

		                <input type="radio" name="optional_course" id="optional_course" class="input" value="Advanced Open Water Course" <?php if (get_user_meta( $current_user_id, 'optional_course', true )== 'Advanced Open Water Course')  {echo 'checked';} ?> />Advanced Open Water Course

		                <input type="radio" name="optional_course" id="optional_course" class="input" value="Adventure Deep Dive" <?php if (get_user_meta( $current_user_id, 'optional_course', true )== 'Adventure Deep Dive')  {echo 'checked';} ?> />Adventure Deep Dive

		                <input type="radio" name="optional_course" id="optional_course" class="input" value="Specialty" <?php if (get_user_meta( $current_user_id, 'optional_course', true )== 'Specialty')  {echo 'checked';} ?> />Specialty
						 </p>
					</div>
					
					<div class="clearfix"></div>
					<div class="col-md-5 edit_profile">
						<input type="submit" class="btn btn-primary pull-right" value="Update" name="profile_update">		
					</div>
				</form>
			</div>
			<?php } else{ wp_redirect(site_url().'/wp-login.php'); } ?>
		</div>
	</div>
</section>

<?php get_footer();